#!/usr/bin/env python
# r3340 - an OSC IRC bot that want to take over the world
# DEPENDS: python-liblo, python-irclib

import liblo, sys
import string, time, random
import threading
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# send to OSC server <port> on localhost
# joins <channel>, as <nickname>
if len(sys.argv) == 5:
    (nickname, server, channel, ports) = (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
else:
    print '\033[1;31mUsage:\033[m r3340.py <nickname> <server> \<irc-channel> <OSC port(s)>'
    print '\033[1;32mexample:\033[m ./r3340.py r3340 irc.goto10.org \#mode+v 57120'
    print '\033[1;32mexample:\033[m ./r3340.py banana localhost \#goto10 9997,9998,9999'
    print 'try also r3340:auto'
    sys.exit(1)

try:
    targets = {}
    for port in ports.split(','):
        targets[port] = liblo.Address(port)

except liblo.AddressError, err:
    print str(err)
    sys.exit("oioioi osc address error")

# we are the robots
class TestBot(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.message = ",."
        
	def on_nicknameinuse(self, c, e):
		c.nick(c.get_nickname() + "_")
    
    def on_welcome(self, c, e):
        c.join(self.channel)
        print "I'm in " + channel
        
	def delay_start_timer(self, c, e, bool):
		time.sleep(30)	# wait 30 seconds before starting auto-mode
		# if cmd == self.msg:
		self.run_timer(c, e, bool)

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) > 1 and irc_lower(a[0]) == irc_lower(self.connection.get_nickname()):
        	cmd = a[1].strip()
        	if cmd == "help":
        		c.notice(self.channel, "feed me ASCII and I'll pass it to OSC port(s) " + ports)
        		c.notice(self.channel, " (use space or _ for a rest)")
        		c.notice(self.channel, "commands:")
        		c.notice(self.channel, "GOTO[n] ie. GOTO8, where n is a prime number (1-13)")
        		#c.notice(self.channel, "rand[n] ie. rand5, idem")

			# single random number size n
#         	elif cmd == "rand1":
#         		c.notice(self.channel, "rand1")
#         		self.make_rand_cmd(e, 1)
#         	elif cmd == "rand2":
#         		c.notice(self.channel, "rand2")
#         		self.make_rand_cmd(e, 2)
#         	elif cmd == "rand3":
#         		c.notice(self.channel, "rand3")
#         		self.make_rand_cmd(e, 3)
#         	elif cmd == "rand5":
#         		c.notice(self.channel, "rand5")
#         		self.make_rand_cmd(e, 5)
#         	elif cmd == "rand8":
#         		c.notice(self.channel, "rand8")
#         		self.make_rand_cmd(e, 8)        		
#         	elif cmd == "rand13":
#         		c.notice(self.channel, "rand13")
#         		#dir(self.make_rand_cmd(e, 13))

			# loop shuffling input runs n times
        	elif cmd == "GOTO1":
        		c.notice(self.channel, "GOTO + 1")
        		threading.Thread(target=self.run_timer(c, e, True, 1)).start()
        	elif cmd == "GOTO2":
        		c.notice(self.channel, "GOTO + 2")
        		threading.Thread(target=self.run_timer(c, e, True, 2)).start()        		
        	elif cmd == "GOTO3":
        		c.notice(self.channel, "GOTO + 3")
        		threading.Thread(target=self.run_timer(c, e, True, 3)).start()
        	elif cmd == "GOTO5":
       
        		threading.Thread(target=self.run_timer(c, e, True, 5)).start()
        	elif cmd == "GOTO8":
        		c.notice(self.channel, "GOTO + 8")
        		threading.Thread(target=self.run_timer(c, e, True, 8)).start()        		
        	elif cmd == "GOTO13":
        		c.notice(self.channel, "GOTO + 13")
        		threading.Thread(target=self.run_timer(c, e, True, 13)).start()
        	
        	else:
        		self.make_command(e, cmd)
        		
		return
        
    def make_command(self, e, cmd):
        nick = nm_to_n(e.source())
        # debug regular user input
        c = self.connection
        c.notice(self.channel, cmd)
        self.send_osc_message(e, cmd)
        self.message = cmd

	def make_rand_cmd(self, e, length=8, chars=string.letters + string.digits):
		c = self.connection
		self.message = ''.join([choice(chars) for i in range(length)])
		c.notice(self.channel, self.message)
        self.send_osc_message(e, cmd)
        self.message = cmd

    def send_osc_message(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
        msg = liblo.Message("/"+nick)
        
        if cmd != None:
        	for i in cmd:
        		msg.add(ord(i))
			for port in ports.split(','):
				liblo.send(targets[port], msg)
        	print nick + ' -> ' + cmd

    def run_timer(self, c, e, bool, amount):
    	count = 0
    	g = "_"
    	d = self.message
    	print bool
    	time.sleep(1)
    	c.notice(self.channel, "<- " + d)
    	time.sleep(2)
    	while count < amount:
    		msgsize = random.randrange(8,20)
    		g = ''.join([random.choice(d) for i in range(msgsize)])
    		c.notice(self.channel, "-> " + g)
    		self.send_osc_message(e, g)
    		time.sleep(random.randrange(3,8))
    		count += 1
    	else:
    		print "timer stopped"
    	
def main():
    bot = TestBot(channel, nickname, server, 6667)
    bot.start()

if __name__ == "__main__":
    main()

