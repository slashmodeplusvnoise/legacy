#!/usr/bin/env python
# syncmaster.py - little OSC master clock
# It should be "good enough" but don't expect uber-accuracy with this one ;)
# DEPENDS: python-liblo
#
#    (c) 2009 - GOTO10
#    This file is part of "/mode+v noise".
#
#    "/mode+v noise" is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    "/mode+v noise" is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with "/mode+v noise".  If not, see <http://www.gnu.org/licenses/>.
#

import liblo, sys, time
targets = {}

# send to OSC server(s) <OSC ports> on localhost
if len(sys.argv) == 3:
    (tempo, ports) = (sys.argv[1], sys.argv[2])
else:
    print '\033[1;31mUsage:\033[m syncmaster.py <tempo> <OSC port(s)>'
    print '\033[1;32mexample:\033[m ./syncmaster.py 126 57120,57121'
    sys.exit(1)

def OSCtest():
    try:
        for port in ports.split(','):
            targets[port] = liblo.Address(port)
    except liblo.AddressError, err:
        print str(err)
        sys.exit("oioioi OSC address error!!!")

def sync0(tempo,ports):
    for port in ports.split(','):
        liblo.send(targets[port], "/sync0", float(tempo))

def main():
    OSCtest()
    delay = (60. / float(tempo))
    while True:
        sync0(tempo, ports)
        # DEBUG
        print tempo
        print delay
        time.sleep(delay)

if __name__ == "__main__":
    main()
