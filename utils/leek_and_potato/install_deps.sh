#!/bin/bash
# quick fix for leek&potato to install mode+v missing deps

sudo apt-get update
sudo apt-get install python-irclib
sudo wget "http://de.archive.ubuntu.com/ubuntu/pool/universe/p/pyliblo/python-liblo_0.7.2-0ubuntu2_i386.deb"
sudo dpkg --install python-liblo_0.7.2-0ubuntu2_i386.deb
