/*
////////////////// show buffers

/// kill named buffer

C-x 4 C-f	find-file-other-window
C-x 4 C-o	display-buffer
C-x 4 .		find-tag-other-window
C-x 4 0		kill-buffer-and-window
C-x 4 a		add-change-log-entry-other-window
C-x 4 b		switch-to-buffer-other-window
C-x 4 c		clone-indirect-buffer-other-window
C-x 4 d		dired-other-window
C-x 4 f		find-file-other-window
C-x 4 m		compose-mail-other-window
C-x 4 r		find-file-read-only-other-window

C-x 5 C-f	find-file-other-frame
C-x 5 C-o	display-buffer-other-frame
C-x 5 .		find-tag-other-frame
C-x 5 0		delete-frame
C-x 5 1		delete-other-frames
C-x 5 2		make-frame-command
C-x 5 b		switch-to-buffer-other-frame
C-x 5 d		dired-other-frame
C-x 5 f		find-file-other-frame
C-x 5 m		compose-mail-other-frame
C-x 5 o		other-frame
C-x 5 r		find-file-read-only-other-frame

///////////////////////////////// E is Emacs
E.perform([\font1, \font2, \font3, \font4].choose);

E.set(font:0, menubar:1,toolbar:0,linum:1, scrollbar:0,lines:1,columns:1)
E.set(font:1, menubar:1,toolbar:0,linum:0, scrollbar:0,lines:1,columns:1)
	// mmmm :)
E.set(font:1, menubar:1,toolbar:0,linum:0, scrollbar:0,lines:0,columns:0)
E.set(font:2, menubar:1,toolbar:0,linum:1, scrollbar:1,lines:1,columns:1)

E.post   // post window to front
E.workspace // WorkSpace to front
E.msg // Emacs/elisp post window 

// show buffer: C-x C-b
//////////////////////////// file magement

// exists; open it, DON'T go there
E.find("/home/a/a-emacs/cmodes/sc.el")
E.find("/home/a/.emacs")

// not existing; make it; go there
E.find("/home/a/blasd.scd")

//switches to a named buffer in another window
E.switch2("*SCLang:WorkSpace*")
E.switch2("sc.el")

E.linum(1)

E.scrollbar(0)
 
///////////////////////
E.font2a(pxlsz:10)

// !!
E.font2b(18)
E.font2c(12)

//////
(key-chord-define-global "jk" 'undo)
(key-chord-define-global ",." "<>\C-b")
////////////////////////////////////
//no
//E.switch // switch to another frame
//E.display("/home/a/perf/a.scd")


// existing, no, makes anew buffer with that name !!
// E.switch2("/home/a/.emacs")

;;(setq scroll-bar-mode-explicit t)
;;(set-scroll-bar-mode `right)
;;(setq scroll-step 3)
 
(global-set-key [vertical-scroll-bar down-mouse-1] 'scroll-bar-drag)

(put 'scroll-left 'disabled nil)

// transient mark: show closures, skip fast through text
// stop blinking 
Emacs.evalLispExpression(['transient-mark-mode', 1].asLispString);
Emacs.evalLispExpression(['blink-cursor-mode', 0].asLispString);
Emacs.evalLispExpression(['blink-cursor-mode', 1].asLispString);


????
Emacs.evalLispExpression(['sclang-font-lock-keywords', 'sclang-font-lock-keywords-1'].asLispString)
Emacs.evalLispExpression(['sclang-font-lock-keywords'].asLispString)
Emacs.evalLispExpression(['sclang-font-lock-keywords-2')
				
Emacs.evalLispExpression(['sclang-font-lock-keywords-3'].asLispString)
Emacs.evalLispExpression(['sclang-update-font-lock'].asLispString)

// Emacs.evalLispExpression(['skeleton-pair-insert', t].asLispString)


E.collapse(0)

// Query Replace

M-%

enter FIND string 
enter REPLACE string

M-SPACE or M-, TO CONTINUE

//
Type Space or `y' to replace one match, Delete or `n' to skip to next,
RET or `q' to exit, Period to replace one match and exit,
Comma to replace but not move point immediately,
C-r to enter recursive edit (C-M-c to get out again),
C-w to delete match and recursive edit,
C-l to clear the screen, redisplay, and offer same replacement again,
! to replace all remaining matches with no more questions,
^ to move point back to previous match,
E to edit the replacement string
*/


E  {

	*initClass{

		Platform.case(

			\linux, {
		                Class.initClassTree(Emacs);
                		this.set
			},
			\osx, { 
				if(Emacs.notNil){
					Class.initClassTree(Emacs);
                	this.set
				}{
					"blub".warn
				}
			}
		);		

	}



	*set{|font, menubar, toolbar, linum, scrollbar, lines, columns|
		if(font.notNil){ this.perform([\font1, \font2, \font3, \font4][font]) }{ this.font1 };
		if(menubar.notNil){ this.menubar(menubar) }{  this.menubar(1) };
		if(toolbar.notNil){ this.toolbar(toolbar) }{  this.toolbar(0) };
		if(linum.notNil){ this.linum(linum) }{ this.linum(0) };
		if(scrollbar.notNil){ this.scrollbar(scrollbar) }{ this.scrollbar(0) };
		if(lines.notNil){ this.lines(lines) }{ this.lines(1) };
		if(columns.notNil){ this.columns(columns) }{ this.columns(1) };
	}

	*collapse{|on=0|
		Emacs.evalLispExpression(['set-selective-display', on].asLispString );
	}
	*msg{ E.switch2("*Messages*") }

	*post{ E.switch2("*SCLang:PostBuffer*") }

	*workspace{ 
		E.switch2("*SCLang:WorkSpace*");
		Emacs.evalLispExpression(['sclang-mode', 1].asLispString );
	}

	*help{ E.switch2("*SC_Help:w3m*") }

	*linum{|on=1|
		Emacs.evalLispExpression(['linum-mode', on].asLispString)
	}

	*font1 {|font| Emacs.evalLispExpression(['set-default-font', "6x10"].asLispString) }	
	*font2 {|font| Emacs.evalLispExpression(['set-default-font', "fixed"].asLispString) }
	*font2a {| pxlsz=10 | Emacs.evalLispExpression(['set-default-font', "-*-*-*-*-*-*-" ++ pxlsz ++ "-70-*-*-*-*-*-*"].asLispString) }
	*font2b {| pxlsz=14|		
		Emacs.evalLispExpression(['set-default-font', "-*-clean-*-*-*-*-" ++ pxlsz ++ "-*-*-*-*-*-*-*"].asLispString) 
	}
	*font2c {| pxlsz=14|
		"10, 11, 12, 14, 17, 19, 19, 20, 24, 25, 26, 34".postcln;
		Emacs.evalLispExpression(['set-default-font', "-*-lucida-medium-r-*-*-" ++ pxlsz ++ "-*-*-*-*-*-*-*"].asLispString) 
	}
	*font3 {|font| Emacs.evalLispExpression(['set-default-font', "-ascender-liberation.mono-medium-r-normal--0-0-0-64-p-0-iso8859-1"].asLispString) }
	*font4 {|font| Emacs.evalLispExpression(['set-default-font', "-ascender-liberation.mono-medium-r-normal--0-0-120-120-p-0-iso8859-1"].asLispString) }



	*menubar{|on=1| Emacs.evalLispExpression(['menu-bar-mode', on].asLispString) }
	*toolbar{|on=0| Emacs.evalLispExpression(['tool-bar-mode', on].asLispString) }

	*lines{|on=1| if(on==0){on=1.neg}; Emacs.evalLispExpression(['line-number-mode', on].asLispString) }
	*columns{|on=1| if(on==0){on=1.neg}; Emacs.evalLispExpression(['column-number-mode', on].asLispString) }
	*scrollbar{|on=0| if(on==0){on=1.neg}; Emacs.evalLispExpression(['scroll-bar-mode', on].asLispString) }

	*switch { // switch to another window
		Emacs.evalLispExpression(['other-window', '1'].asLispString);
	}

	*display {| path | // display a buffer on another window (but don't move there
		Emacs.evalLispExpression(['display-buffer', path].asLispString);
	}
  
	*find {| path | // if already open; move there
		// load a file in a buffer, display in another window, 
		Emacs.evalLispExpression(['find-file-other-window', path ?? "/home/a/.emacs"].asLispString);
	}
	
	*switch2{| name | // switch to a named buffer or
		// switch to another window, and load a (new?) buffer with that name in it
		Emacs.evalLispExpression(['switch-to-buffer-other-window', name ? "*SCLang:PostBuffer* *"].asLispString);
	}
	*transient{|on=1|
		Emacs.evalLispExpression(['transient-mark-mode', on].asLispString);
	}

	*blink{|on=1|
		Emacs.evalLispExpression(['blink-cursor-mode', on].asLispString);
	}
	
	
}





/*

????
Emacs.evalLispExpression(['sclang-font-lock-keywords', 'sclang-font-lock-keywords-1'].asLispString)
Emacs.evalLispExpression(['sclang-font-lock-keywords'].asLispString)
Emacs.evalLispExpression(['sclang-font-lock-keywords-2')
				

Emacs.evalLispExpression(['sclang-font-lock-keywords-3'].asLispString)
Emacs.evalLispExpression(['sclang-update-font-lock'].asLispString)



	///////////////////////////////


5x7 5x8
6x10 6x12 6x13 6x13bold 6x9
7x13 7x13bold 7x13euro 7x13eurobold
7x14 7x14bold
8x13 8x13bold 8x16 8x16kana 8x16romankana
fixed hanzigb16fs
lucidasans-8 lucidasans-10 lucidasans-12

// kill a named buffer
Emacs.evalLispExpression(['kill-buffer', "emacs.scd"].asLispString)
Emacs.evalLispExpression(['kill-buffer', "*Disabled Command*"].asLispString)


////////////////////////////////////////////////////////////
// display a buffer on another window (but don't move there)
Emacs.evalLispExpression(['display-buffer', "a.scd"].asLispString);

// show the post buffer in a window
Emacs.evalLispExpression(['sclang-show-post-buffer', '1'].asLispString);

//// switch to another window
Emacs.evalLispExpression(['other-window', '1'].asLispString);


// load a file (not loaded in a buffer) in another window, if already open; move there
Emacs.evalLispExpression(['find-file-other-window', "c.scd"].asLispString);
Emacs.evalLispExpression(['find-file-other-window', "/home/a/.emacs"].asLispString);

// load a file in on another window, and move there
Emacs.evalLispExpression(['find-file-other-window', "c.scd"].asLispString);
Emacs.evalLispExpression(['find-file-other-window', "record.scd"].asLispString);
Emacs.evalLispExpression(['find-file-other-window', "b.scd"].asLispString);


// switch to another window, and load a (new?) buffer in it
Emacs.evalLispExpression(['switch-to-buffer-other-window', "*SCLang:PostBuffer* *"].asLispString);
Emacs.evalLispExpression(['switch-to-buffer-other-window', "c.scd"].asLispString);

*/