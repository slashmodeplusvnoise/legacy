/*
does respond to general tag "/bs1"

todo: loading new sounds through .synth (functions as wall as synthdefs does not work)
*/

BotSynth {
	classvar <>verbose=false, <>botSynths, <masterClock, <>shfl=0, <>synthDefNames;
	var <server, <clock, resp, <storedTempo=120, <sy, <>pattern, <task, <oscTag, <id;
	var <argSynthDefName="fm1", <>stepSynC=true, cond, testForKeywords=true;
	var <pattlength, <botPid;

	// class methods
	*initClass {
		botSynths = ();
		synthDefNames = List.new;
		masterClock = TempoClock(2);
		masterClock.permanent_(true);
	}
	
	*new {|serv|  
		^super.new.init(serv) 
	}

	*sync {|stepSync, tempo|
		botSynths.do{|bs| bs.sync(stepSync, tempo) }
	}
			
	// instance methods
	init {|serv, channel, ports|
		
//		botPath = botPath ? "~/Desktop/mode+v/bots/r3340/r3340.py".standardizePath;
//		botName = botName ? "r33401-" ++ Date.seed;
		channel = channel ? "#mode+v"; 
		ports = ports ?"57120,57121,57122";
//		botPid = this.startBot(botPath, botName, channel, ports);
		
		cond = Condition.new;
		server = serv ? Server.default;
		server.boot;


		//\bs1
		oscTag = "/bs1".asSymbol;


		clock = TempoClock.new(storedTempo);
		pattern = [70,60,0,0,71,63,0,0,0].sputter(0.25,rrand(3,12)); 
		pattlength = pattern.size;
		//.scramble.pyramid(rrand(1,3))

		Post << nl << "NetAddr.langPort: " << NetAddr.langPort << nl << nl;

		this.oscResp(oscTag);
		
		Routine.run{
			server.bootSync(cond);
			this.loadDefs(cond);
			server.sync(cond);
			this.synth; // no synthdefname arg input: defaults to fm1 (see synth method)
		};

		// add to global dictionary (retrieve with: BotSynth2.botSynths)
		id = (oscTag ++ Date.getDate.asSortableString).asSymbol;
		botSynths.put(id, this);
		CmdPeriod.add({this.free});
	}

	oscResp {|oscTag|
		// nick = nick ?? oscTag;
		if( resp.notNil ){
			resp.remove;
			this.oscResp 
		}{
			resp = OSCresponderNode(nil, "/bs1" /*oscTag*/, {|t,r,msg|
				if(verbose==true){ 
					Post << nl << t << "\t" << msg;
				};
		
				msg = msg.copyToEnd(1);
				
				("\nMESSAGE:  " ++ msg).post;
				
				if(msg[0] == 115){
					// stepsync if 1st character is 's'
					msg = msg.drop(1);
					this.sync(true);
				}
				{
					// barsync if 1st character is 'b'
					if(msg[0] == 98 ){		
						msg = msg.drop(1);
						this.sync(false);
					}{
						/*// test for synth messages, todo: tempo
						if(this.patternToString(msg) == "synth:"){
							Post << msg << "\tnot yet implemented" << "!";
						};*/
					}
				};
				pattern = msg;
				pattlength = pattern.size;
					
				Post << nl << "thisBotSynth: " << oscTag << nl <<					"pattern: " << pattern << nl << "pattlength: " << pattlength << nl;

			}).add;
		};
		
		// oscTag = nick;
	}
	
	// change pattern
	filter {|find, replace|
		pattern = pattern.collect{|item,i| item.replace(find, replace) };
		^pattern
	}

	sync {|stepSync, myNewTempo, argClock|
		stepSync = stepSync ? stepSynC;
		stepSynC = stepSync;
		
		if(myNewTempo.notNil){ 
			storedTempo = myNewTempo;
		};

		// clear the scheduler, and start a new process 
		// (syncing either to the given clock, or to a new clock)
		// if(clock.notNil){ clock.clear };

		if( clock.notNil ){ clock.clear };
		if( argClock.notNil){ clock = argClock }
		{ 		
			//	<all, <>default;
			//
			//	var <queue, ptr;
			//	
			//	var <beatsPerBar=4.0, barsPerBeat=0.25;
			//	var <baseBarBeat=0.0, <baseBar=0.0;
			//	var <>permanent=false;
			// 	*new { arg tempo, beats, seconds, queueSize=256 }		
			clock = TempoClock.new(storedTempo) 
		};
		
		this.run(stepSync, myNewTempo)
	}

	waitForSync {|stepSync=true, tempo| // for the syncmaster
		OSCresponderNode(nil, "/sync0", {|time,rsp,msg|
			if(clock.notNil){ clock.stop };
			clock = TempoClock.new;
			this.run(stepSync, msg[1] ? tempo);
			"just received a syncmaster sync signal".postln;
		}).add.removeWhenDone
	}
	
	synth {|input|
		var synthDefName, synthFunction;
		
		if(input.isNil){ input = \fm1 };
		input.class.postcln;
		
		
		if( input.isKindOf(String) or: { input.isKindOf(Symbol) } )
		{
		    	argSynthDefName = synthDefName;
			synthDefName = input; // ? argSynthDefName ? "fm1";

			synthDefNames.add(synthDefName);
		    	sy.set(\amp,0);

			server.waitForBoot({
				sy.free;
				server.sync(cond);
				sy = Synth(synthDefName.asSymbol);
				server.sync(cond);
				this.reset(stepSynC);	
			})
		}{		
			if(input.isKindOf(Function)){
				server.waitForBoot({	
					synthFunction = input ? { |freq| SinOsc.ar(freq) };
	
					synthDefName = \wrapped ++ Date.getDate.asSortableString;
					synthDefNames.add(synthDefName);
					SynthDef( synthDefName, { |out|
						Out.ar(out, SynthDef.wrap( synthFunction ))
					}).memStore;
	
					sy.free;
					server.sync(cond);
					sy = Synth(\wrapped);
					server.sync(cond);
					this.reset(stepSynC);					
					
				})
			}{
				if(input.isKindOf(NodeProxy)){
					server.waitForBoot({

						synthFunction = input.source;
						sy.free;
						server.sync(cond);
						sy = input.play;
						// sy = Synth(\wrapped);
						server.sync(cond);
						this.reset(stepSynC);								server.sync(cond);
						// Post << nl << oscTag << ": " << synthDefName << nl;
						server.queryAllNodes;
			
					});
				
				}{ 
					"no valid input,\nonly synthdefnames and synthfunctions are valid".warn 
				}
			}
		};
				
	}
	
	cycleSynths {|run=true, which=0|
		if(run){
			Tdef(\cycler,{
				inf.do{|i|
					which = i % synthDefNames.size;  
					this.synth(synthDefNames[which]);
					4.rrand(20).wait;
				}
			}).play	
		}{
			Tdef(\cycler).stop;
		}
	}	

	run {|stepsync=true, argTempo=120|
 		var ii, jj, delta=0.25, step=0, waitForSyncTime;
		
		storedTempo = argTempo ?? storedTempo;
		clock.tempo = storedTempo / 60;
		
		stepsync = stepsync ? stepSynC;
		
		waitForSyncTime = (clock.beats.roundUp - clock.beats);
		// Post << nl << "waitForSyncTime: " << waitForSyncTime;
		
		clock.sched( waitForSyncTime, {
			if( stepSynC ){
				// stepsync
				if( shfl.notNil ){
					if( step%2==0 ){ delta = 0.25 + shfl }{
						delta = 0.25 - shfl
					}
				}{ delta=0.25 }
			}{	
				// barsync
				delta = (clock.tempo.reciprocal  * 8) / pattlength
			};
			
			jj = step % pattlength;
			
			server.bind{
				if( not(pattern[jj] == 95 or: {pattern[jj] == 32}) ){
					sy.set(\t_trig, 1,\freq, (pattern[jj]).midicps);
					// (pattern[jj]).post; " ".post;
				}
			};
			step = step+1;
			delta
		});
	
/*		Post << oscTag << " running..." << nl << "stepsync: " << stepsync << nl <<
			"clock.tempo: " << clock.tempo << nl;
*/

	}

	run2 {|stepsync=true, argTempo=126|
 		var jj, delta=0.25, step=0, barSyncDelta;
		
		// iterate over patternlist (which was filled by the bot)
		task = Tdef(oscTag.asSymbol, {
			inf.do{|count|
				jj = count % pattern.size;
				barSyncDelta = clock.tempo.reciprocal  * 8 / pattern.size;

				if(pattern.notNil){
					if(verbose){
						if( count % pattern.size == 0){"".postln}{" ".post};
						if(pattern[jj] == 32){ Post << "_"; }
							{
								server.bind{ 
									sy.set(\t_trig, 1,\freq, (post(pattern[jj])).midicps) 
								}
							}
					}{
						if(pattern[jj] == 32){/* rest */} 
							{ server.bind{ sy.set(\t_trig, 1,\freq, (pattern[jj]).midicps); } }
					}
				};
				// stepsync
				if(stepsync){ 0.25.wait}
				// barsync
				{					
					if(verbose){ barSyncDelta.post };
					barSyncDelta.wait
				}
			}
		}).play(clock, true, [16,0])
	}

	reset { this.run; ("reset:" ++ oscTag).postln }

	free {
		clock.clear;
		// clock.stop;
		resp.remove;
		botSynths.removeAt(id);
		sy.free;
		// this.killBot;
	}
	
	patternToString {|pattern|
		var string="";
		if(pattern.isKindOf(Array))
		{
			pattern.do{|item, i|	
				string = string ++ item.asAscii;
			};
			^string
		}{
			"input not an Array".warn
		}
	}

	loadDefs {|synthDefName, cond|

		SynthDef(\fm1, {| outBus=0, t_trig=0, freq=1040, dcy=0.2, amp=0.3|
			var snd, env = EnvGen.ar(Env.perc(0.01, freq.reciprocal * 200 * dcy ), t_trig, doneAction:0);
			snd = PMOsc.ar(freq, freq*0.5, 2*TExpRand.kr(0.5, 4, t_trig),[0, SinOsc.kr(0.3,0,pi)]);
			OffsetOut.ar(outBus, snd * env * amp);
		}).send(server);

		SynthDef(\fm2, {| outBus=0, t_trig=0, freq=1040, dcy=0.09, amp=0.2|
			var snd, env = EnvGen.ar(Env.perc(0.01,dcy*LFNoise2.kr(0.1,0.1,1)), t_trig, doneAction:0);
			snd = RLPF.ar(
					PMOsc.ar(freq, freq*[1.5, 2, 1].choose, TRand.kr(0.5, 3, t_trig),0),
					SinOsc.kr([0.1,0.11]).range(400, 1900),
					0.4
			);
			OffsetOut.ar(outBus, snd * env * amp);
		}).send(server);
	
		SynthDef(\anasynth1, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, adcy=0.3, pdcy=0.07,
			ffreq=700, rq=0.3, amp=0.2|
			var aenv, penv, osc1, osc2, filtd;
			aenv = EnvGen.ar(Env.perc(0.01, rrand(0.02,adcy)), t_trig, doneAction: 0);
			penv = 1 + EnvGen.ar(Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
			pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
			sfreq = freq;
			pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1);
			osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
			osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
			filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
			OffsetOut.ar(0, filtd * aenv * amp * 0.8)
		}).send(server);	

		SynthDef(\anasynth2, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, adcy=0.3, 
			pdcy=0.07, ffreq=700, rq=0.3, amp=0.14|
			var aenv, penv, osc1, osc2, filtd;
			aenv = EnvGen.ar(Env.perc(0.01, rrand(0.02,adcy)), t_trig, doneAction: 0);
			penv = 1 + EnvGen.ar(Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
			pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
			sfreq = freq  * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
			pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1) * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
			osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
			osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
			filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
			OffsetOut.ar(0, filtd * aenv * amp * 0.8)
		}).send(server);

		SynthDef(\procgain, {|in=0, out=0, gain=2, amp=0.2|
			Out.ar(out, 
				Pan2.ar((In.ar(in)*gain).softclip * amp , 0.2 * SinOsc.kr)
			)
		}).send(server);
		
		SynthDef(\procfdly, {|in=0, out=0, ffreq=1000, time=0.5, rls=2, rq=0.6, mix=0.3, amp=0.9|
			var dry;
			in = In.ar(in);
			dry = in;
			in = in + CombC.ar(in, 2*time, time, time*rls);
			in = RLPF.ar(in, ffreq, rq); //.round(rnd);
			in = (dry * (1-mix)) + (in*mix);
			Out.ar(out, Pan2.ar(in * amp , 0))
		}).send(server);

		synthDefNames.add(\fm1);
		synthDefNames.add(\fm2);
		synthDefNames.add(\anasynth2);
		synthDefNames.add(\anasynth1);


	}
	
}
