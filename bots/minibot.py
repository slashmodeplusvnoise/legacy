#!/usr/bin/env python
# minibot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# minibot config

channel = "#mode+v"
nickname = "mini"
server = "irc.gosub10.org"
IRCport = 6667
OSCport = 57121
mybotsynth = "/h4nk"

# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        # master gets his message sent to OSC directly...
        if nick == "master":
            msg = liblo.Message(self.mybotsynth)
            for i in cmd:
                    msg.add(ord(i))
            self.is_Preset(e, c, cmd)
            liblo.send(self.target, msg)
            r3340.notify(1, nick + ':' + cmd)
               

        # ...but super's messages are scried first.
        elif nick == "super":
            msg = liblo.Message(self.mybotsynth)
            
            string = r3340.cervelle(cmd)
            string.scry(100)

            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)

            r3340.notify(1, nick + ':' + string.message)

        elif cmd == "help":
            c.notice(self.channel, "I'm helpful")

        elif cmd == "yo" and nick == "bob":
            c.notice(self.channel, "I'm helpful, bob")

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)

    #patternDict = ()

'''
echo store a pattern with:
(tip: use Control-a to go to the beginning of the line
// name pattern:
store: funky1 93039382,,,....
load: funky1
'''

    def storePattern(self, c, e, command)
        c.notice(self.channel, "storing")
        #echo "stored"
        #name = input.split($ )[0]
        #pattern = input.split($ )[1]
        #patternDict[name] = pattern
    
    def loadPattern(name)
        msg = patternDict[name]

    def isPreset(self, c, e, cmd)
        c.notice(self.channel, "ispreset enter")
        r3340.notify(1, 'ispreset:' + cmd)

        '''        
        if cmd s first five chars are "load:"
            c.notice(self.channel, "is load:")
            loadPattern(name)

        if cmd s first six chars are "store:"
            c.notice(self.channel, "is store:")
            storePattern(cmd)
        '''

    #if cmd = any.of.names.in.patternDict
    #    loadPattern(name)



# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
