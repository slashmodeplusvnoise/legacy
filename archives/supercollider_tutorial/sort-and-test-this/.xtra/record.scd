
(~path +/+ "home.scd").openTextFile;

//// write supercollider output to a file

// preparations
s.recHeaderFormat="wav";
s.recSampleFormat="int16";
s.prepareForRecord("~/media/sc_recordings/ringtone1.wav".standardizePath); // enter a path / name


// start recording
s.record;

// play something
x = {|a=1,b=1,c=1| c=Lag3.kr(c,LFNoise0.kr(0.25,0.11,0.1)); SinOsc.ar(LFNoise1.kr(a*[12,30],1200,c*1300), SinOsc.ar(LFNoise0.kr(b*[2,3],200,c*300)), 0.3)};

y = x.play;

y.set(\a, 1,\b, 1,\c, 1);

// some variation
fork{ 10.do{ y.set(\a, 2.0.exprand(0.1),\b, 2.0.exprand(0.1),\c, 2.0.exprand(0.1)); rrand(0.2,7).wait}};
fork{ 10.do{ y.set(\a, 10.0.exprand(0.0001),\b, 10.0.exprand(0.001),\c, 10.0.exprand(0.1)); rrand(0.2,7).wait}};

// stop the synth
y.free;

// stop recording
s.stopRecording;

// check the file
"aplay ~/media/sc_recordings/ringtone1.wav&".unixCmd





////////////////////////////////////////////////////
/*
s.prepareForRecord("~/botsynth_out01.wav".standardizePath); // enter a name

(
s.waitForBoot{
	a = BotSynth2.new(s, nick:"/jk", synthDefName:"fm1");
	b = BotSynth2.new(s, nick:"/ps", synthDefName:"anasynth2") 
})


// start recording
s.record;

BotSynth2.sync(tempo:110)

// some variation, change one line at a time
a.pattern_("ABCABCABC,,,,,,,,,..........,,,,".ascii)
b.pattern_("#@@@@$#######<<<<<<,,,,,..........0_oooo__".ascii)
b.pattern_(",,,,....###,,,,".ascii)
a.pattern_(",,,,....###%%%@".ascii)
b.synth(\fm2)
a.synth(\anasynth1)
b.pattern_("w000000syuuuuuuuuABCDEFuu___GHGHGHIJK".ascii)

// free the synths
a.free; b.free;

// stop recording
s.stopRecording

// check the file
"aplay ~/botsynth_out01.wav&".unixCmd
*/