# Mini Python tour

# Emacs command for Python interpreter
# Tools Menu, Python, Start Interpreter
# C-c C-r  Eval Region, in python workspace buffer
# C-up C-down command history, in interpreter buffer

# switch on the menu bar if you don't have it with:
# M-x menu-bar-mode 1

# strings
print 'hello'
print 'I want' + ' a cheezburger'
print 'I want' * ' a cheezburger'

# integers and floats
print 1.5
print 1 + 1.1

# not good
print 'I want' + 5 + 'kroketen'
# good
print 'I want ' + str(5) + ' kroketen'
print 'I want a vewy pwetty kitty\n' * 50

# variables
fruit = 'banana'
quantity = 1
space = ' '
print str(quantity) + space + fruit

# modules
print sqrt(4)       # nope ...
import math         # ah ah !
print math.sqrt(4)

# functions
def beer():
    print 'I want ' + str(5) + ' beers!'

beer()

# functions argument
def beer(quantity):
    print 'I want ' + str(quantity) + ' beers!'

beer(1000)
beer(12)

def order(quantity, what):
    print 'I want ' + str(quantity) + ' ' + what + '!'

order(200, 'moustaches')

# conditions
def order(quantity, what):
    if quantity == 1:
        print 'I want ' + str(quantity) + ' ' + what + '!'
    elif quantity == 0:
        print "I don't want any " + what
    else:
        print 'I want ' + str(quantity) + ' ' + what + 's!'

order(20, 'banana')
order(1, 'banana')
order(0, 'banana')


# loops
def order(quantity, what):
    if quantity == 1:
        print 'I want ' + str(quantity) + ' ' + what + '!'
    else:
        for i in range(quantity):
            print 'I want ' + str(quantity) + ' ' + what + 's!'
            print i

order(5, 'cake')

###

# Open Sound Control


# we need some more modules
import liblo
import sys


# safety 1st
try:
    target = liblo.Address(1111)
except liblo.AddressError, err:
    print str(err)
    sys.exit()


# Now is a good time to start dumpOSC.py

# building and sending OSC messages
def spam():
    # build "/viagra buy me!"
    msg = liblo.Message("/viagra")
    msg.add("buy me!!!")
    liblo.send(target, msg)

spam()

def eggs(quantity):
    for i in range(quantity):
        msg = liblo.Message("/viagra")
        msg.add(i)
        liblo.send(target, msg)

eggs(12)

