#!/usr/bin/env python
# copycat.py - INCOMPLETE
# copycat is a bot that will remember the list of other bots and users 
# nicknames and their latest action. It will then regularly attempt to spoof 
# them.
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr


# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.server = server
        self.channel = channel

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        # master gets his message sent to OSC directly...
        if nick == "master":
            msg = liblo.Message(self.mybotsynth)
            for i in cmd:
                    msg.add(ord(i))
            liblo.send(self.target, msg)
            r3340.notify(1, nick + ':' + cmd)
   
        # ...but super's messages are scried first.
        elif nick == "super":
            msg = liblo.Message(self.mybotsynth)
            
            string = r3340.cervelle(cmd)
            string.scry(100)

            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)

            r3340.notify(1, nick + ':' + string.message)

        elif nick == "bob":
            msg = liblo.Message(self.mybotsynth)
            
            string = r3340.cervelle(cmd)
            string.scry(100)
            string.remix()
            string.mushroom()

            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)

            r3340.notify(1, nick + ':' + string.message)

            time.sleep(5l)

            string.scry(100)
            string.remix()
            string.mushroom()       
            


            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)



            r3340.notify(1, nick + ':' + string.message)

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)


# minibot config

channel = "#mode+v"
nickname = "para"
server = "irc.goto10.org"
IRCport = 6667

# minibot go!

minibot = mini(channel, nickname, server, IRCport)
minibot.start()
