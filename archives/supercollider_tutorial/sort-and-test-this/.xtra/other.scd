(~path +/+ "home.scd").openTextFile;
////

// helpfiles for all sc objects and sc related topics
http://supercollider.svn.sourceforge.net/viewvc/supercollider/trunk/build/Help/Help.html
// or (overview):
http://akustik.hfbk.net/sc/SuperColliderHelp

// ugens (with examples)
(~path +/+ "ugen_tour.scd").openTextFile

// two tutorials
http://danielnouri.org/docs/SuperColliderHelp/Tutorials/Getting-Started/Getting%20Started%20With%20SC.html
http://danielnouri.org/docs/SuperColliderHelp/Tutorials/Mark_Polishook_tutorial/Introductory_tutorial.html
