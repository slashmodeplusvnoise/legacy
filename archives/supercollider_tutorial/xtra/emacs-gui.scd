(~path +/+ "home.scd").openTextFile;
(~path +/+ "emacs-sclang.scd").openTextFile;



// Emacs gui examples, in progress

EmacsClassBrowser.new( EmacsBuffer );

// make an emacs-gui on another window

(
p.free;
Emacs.evalLispExpression(['other-window', '1'].asLispString); 
p = EmacsBuffer.new;
Emacs.evalLispExpression( p.use( [ 'sclang-mode' ] ).asLispString );
t = EmacsText( p, "move up/down with arrow keys and press RET, write/evaluate code", 60, \left );
p.newline;
p.newline;
p.editableField( " ", "fork{ 7.do{|i| i.post; 0.125.wait}}", { |v| "\n".post; v.compile.value } );
p.newline;
p.newline;
p.editableField( " ", "fork{ 16.do{|i| 2.pow(i).post; Char.tab.post; 0.25.wait}}", { |v| "\n".post; v.compile.value } );
p.newline;
p.newline;
p.front;
Emacs.evalLispExpression(['sclang-show-post-buffer', '1'].asLispString);
Emacs.evalLispExpression(['sclang-clear-post-buffer'].asLispString);
)


/////////////////////////
// note: making the buffer a sclang-mode buffer to write code in
Emacs.evalLispExpression( p.use( [ 'sclang-mode' ] ).asLispString );

// n = EmacsNumber.new( p, "number box", [0,5].asSpec, { |v| v.postln; } ); // args: buffer, tag/label, spec, action
// x = EmacsButton( p, [ "on", "off", "in between" ], { |v| v.postln; }, "******", "+++++" ); // args: buffer, states, action, prefix, suffix

// b = EmacsPushButton( p, "hello" ).action_( { "do it".postln; } );
// ?? does not always work p.closeButton;

/////////////////////////

(
p = EmacsBuffer.new;
p.defineKey( "hello", { "hey there".postln; } );
p.insert( "this is a really interesting text to read in the buffer" );
p.newline;
p.button( [ "on", "off", "in between" ], { |v| v.postln; } );
p.button( [ "on", "off", "in between" ], { |v| v.postln; }, "******", "+++++" );
p.closeButton; // here it does work ... 
)



//
(
p = EmacsBuffer.new; // args: name
n = EmacsNumber.new( p, "number box", [0,5].asSpec, { |v| v.postln; } ); // args: buffer, tag/label, spec, action
x = EmacsButton( p, [ "on", "off", "in between" ], { |v| v.postln; }, "******", "+++++" );
// args: buffer, states, action, prefix, suffix
p.newline;
t = EmacsText( p, "helloooo", 30, \right ); // args: buffer, string, size, align
p.newline;
e = EmacsEditableField( p, "edit field", "edit me" ).action_( { |v| v.postln; } );
b = EmacsPushButton( p, "hello" ).action_( { "do it".postln; } );
p.front;
)

// you can disable things:
b.enabled_( false );
p.front;
