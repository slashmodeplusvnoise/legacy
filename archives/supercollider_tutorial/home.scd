/*=========================================================================
sc for mode+v noise
=========================================================================

- about Emacs, SuperCollider and mode+v

- Emacs is a macro text editor, which gives lots of control over how code is represented and interacted with.

- SuperCollider Server (aka. SC3) is a versatile environment for audio and music programming,
	there's a client/language/interpreter: SCLang, and a server/synthesizer/renderer: scsynth.

- mode+v noise is about making sound/music with the chat interface
*/

// sc intro
"~/Desktop/mode+v/workshop/supercollider_tutorial/sc_intro.scd".standardizePath.openTextFile;

/*
- BotSynth2 -
 the botsynth can be played by the irc bot
 a pattern can be given directly for testing
 the CHAR "_" (95) has the special meaning of rest ie. no trigger

*/

// preparations:
// check: if not 57120, then change this in the minibot.py config too
NetAddr.langPort;

// run this line if you didn't already add the mode+v/sc path to your .sclang.cfg
"~/Desktop/mode+v/workshop/supercollider_tutorial/config.scd".loadPath;


// start a bot synth (start server first if needed)
s.boot;
if(d.notNil){d.free};
d = BotSynth2(s, botSynthName:"abc", synthDefName:"drum1");

# in minibot.py:
nickname = "minibot0"
OSCport = 57122
mybotsynth = "abc"

// start a bot on the chat server (irc.goto10.org:6667 /join #mode+v)
"~/Desktop/mode+v/bots/minibot.py".standardizePath.unixCmd;
// "~/Desktop/mode+v/bots/minibot.py".standardizePath.openTextFile;

// open a chat in a browserwindow
"firefox http://pjirc.goto10.org/?channel=mode+v?nick=master".systemCmd;

// change settings
d.sy.set(\amp, 0.5, \dcy, 0.5)
d.synth(\drum2)
d.synth(\drum1)
d.sy.set(\amp, 0.5, \dcy, 0.1, \fdcy, 0.1, \freqMul, 10)
d.sy.set(\amp, 0.5, \dcy, 0.2, \fdcy, 0.01, \freqMul, 2)

// change synth
d.synth(\ana2)
d.sy.set(\amp, 0.4, \ffreq, 1000, \rq, 0.1)
d.sy.set(\amp, 0.4, \ffreq, 4000, \rq, 0.9)

d.synth(\drum1)
d.synth(\fm2)

// change pattern
d.pattern_([40, 70, 32, 32, 32, 95, 55, 59])
d.pattern_([40, 70, 32, 32, 32, 95, 55, 67, 65])
d.pattern_([ 115, 108, 46, 46, 108, 108, 115, 46, 46, 46 ])

// transpose (in semitones)
d.transp_(-17)

// and limit range with pmod (round) and transp
d.pmod_(4).transp_(36)
d.pmod_(127).transp_(0)

// check what you'd need to type in the chat box
d.patternToString([97, 109, 73, ])

// only for recent sc ( > 3.1 ?): the reverse
Main.version
d.pattern_("amI.,.__..ABC..__,,".ascii);

// not yet: check this:
// d.pattern.filter(find:"m" , replace:"A")

// stepsync / barsync
d.stepSynC_(false)
d.pattern_("amI.,...____________zz".ascii);
d.pattern_("..x,x,.s......................ssssssssssssoiiiiiiiiiiiiiiiiiiii".ascii);
d.stepSynC_(true)
d.pattern_("..".ascii);

// free the botsynth
d.free

--

