#!/bin/bash
# script that starts all the craftivism bots

(~/Desktop/mode+v/bots/craftivism.bezbot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.rhythmbot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.fleabot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.melodybot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.strangerbot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.timebot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.hungoverbot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.boredbot.py 127.0.0.1)&
sleep 5
(~/Desktop/mode+v/bots/craftivism.enyabot.py 127.0.0.1)&
