BotSynth3 {

	classvar <>botSynths, masterClock, storedTempo=120;
	var <server, <clock, resp, <>verbose=false, <sy, <>pattern, <task, <oscTag, shfl=0;
	var <argSynthDefName="fm1", <>stepSynC=true, cond, testForKeywords=true;

	*initClass {
		botSynths = ();
		masterClock = TempoClock.default;
	}
	
	*new {|serv, nick, synthDefName="fm1"| 
		^super.new.init(serv, nick, synthDefName) 
	}

	*sync{|stepSync=true, tempo|		
		if(tempo.notNil){ storedTempo = tempo };
		botSynths.do{|it|
			/* it.waitForSync(stepsync)*/ it.sync(stepSync, storedTempo)
		}
	}
	
	init {|serv, nick, synthDefName|
		cond = Condition.new;
		server = serv ? Server.default;
		server.boot;
		oscTag = nick.asSymbol;
		clock = TempoClock.new;
		pattern = [70,60,0,0,71,63,0,0,0].sputter(0.25, rrand(3,12)); //.scramble.pyramid(rrand(1,3))

		if((resp.notNil)){}{
			resp = OSCresponderNode(nil, nick, {|t,r,msg|
				if(verbose==true){ Post << nl << t << "\t" << msg; };
				msg = msg.copyToEnd(1);

				if(msg[0] == 98 /*and:{ msg[1] == 98 }*/) {	// stepsync if 1st letter is s
					//msg = msg.drop(1); 
					this.sync(false);
					//super.sync(true, storedTempo ? 130)
				}{
					if(msg[0] == 115 /*and: { msg[1] == 98}*/){	// barsync if 1st letter is b
						//msg = msg.drop(1); 
						this.sync(true)
					}{
						/*
						// test for synth messages, todo: tempo
						if(this.patternToString(msg) == "synth:"){
							Post << msg << "\tnot yet implemented" << "!";
						};
						*/
						pattern = msg;

					}
				};
				
			}).add;
		};

		Routine.run{
			server.bootSync(cond);
			this.loadDefs(cond);
			server.sync(cond);
			this.synth(synthDefName)
		};

		botSynths.put(oscTag, this); // add to global dictionary
	}

	sync {|stepSync=true, myNewTempo=120|
		stepSynC = stepSync;
		if(clock.notNil){ clock.stop; clock.clear };
		clock = TempoClock.new( ((myNewTempo ?? storedTempo) / 60));
		this.run(stepSync);
	}

	waitForSync {|stepSync=true, tempo| // for the syncmaster
		OSCresponderNode(nil, "/sync0", {|time,rsp,msg|
			if(clock.notNil){ clock.stop; clock.clear };
			clock = TempoClock.new;
			this.run(stepSync, tempo ? msg[1]);
			".".postln;
		}).add.removeWhenDone;
	}



	synth {|input|
		var synthDefName, synthFunction;
		if( input.isKindOf(String) or: { input.isKindOf(Symbol) } )
		{
			synthDefName = input ? argSynthDefName ? "fm1";
		    argSynthDefName = synthDefName;
		    sy.set(\amp,0);
			
			server.waitForBoot({
				sy.free;
				server.sync(cond);

				// sy = Synth(synthDefName.asSymbol);
				sy = NodeProxy.audio(server, 2);


				server.sync(cond);
				this.reset(stepSynC);	
			})
		}{		
			if(input.isKindOf(Function)){
				server.waitForBoot({
					synthFunction = input ? { |freq| SinOsc.ar(freq) };

					SynthDef(\wrapped, { |out|
						Out.ar(out, SynthDef.wrap( synthFunction ))
					}).memStore;
	
					sy.free;
					server.sync(cond);
					sy = Synth(\wrapped);
					server.sync(cond);
					this.reset(stepSynC);					
				});
				
			}{ "no valid input,\nonly synthdefnames and synthfunctions are valid".warn }
		}			
	}

	run {|stepsync=true, argTempo=126|
 		var ii, jj, delta=0.25, step=0;
		//Post << "argTempo:" << argTempo << nl << "clock.tempo" << clock.tempo << nl;

		clock.sched(0,{
			if(stepsync){
				if(shfl!=0){
					if(step%2==0){ delta = 0.25 + shfl}{ delta = 0.25 -shfl}
				}
				{ 
					delta=0.25 
				}
			}{ 
				delta = (clock.tempo.reciprocal  * 8) / pattern.size 
			};
			
			jj = step % pattern.size;
			server.bind{
				if(pattern[jj] == 95 or: {pattern[jj] == 32}){/* rest */} 
				{ sy.set(\t_trig, 1,\freq, (pattern[jj]).midicps) }
			};
			step = step+1;
			delta
		});
	}

	run2 {|stepsync=true, argTempo=126|
 		var jj, delta=0.25, step=0, barSyncDelta;
		
		// iterate over patternlist (which was filled by the bot)
		task = Tdef(oscTag.asSymbol, {
			inf.do{|count|

				jj = count % pattern.size;

				barSyncDelta = clock.tempo.reciprocal  * 8 / pattern.size;


				if(pattern.notNil){
					if(verbose){
						if( count % pattern.size == 0){"".postln}{" ".post};
						if(pattern[jj] == 32){ Post << "_"; }
							{ server.bind{ sy.set(\t_trig, 1,\freq, (post(pattern[jj])).midicps) } }
					}{
						if(pattern[jj] == 32){/* rest */} 
							{ server.bind{ sy.set(\t_trig, 1,\freq, (pattern[jj]).midicps); } }
					}
				};
				// stepsync
				if(stepsync){ 0.25.wait}
				// barsync
				{					
					if(verbose){ barSyncDelta.post };
					barSyncDelta.wait
				}
			}
		}).play(TempoClock.default, true, [16,0])
	}

	reset {	this.run }

	free {
		clock.stop;
		clock.clear;
		sy.free;
		resp.remove;
		botSynths.removeAt(oscTag)

	}

	patternToString {|pattern|
		var string="";
		if(pattern.isKindOf(Array))
		{
			pattern.do{|item, i|	
				string = string ++ item.asAscii;
			};
			^string
		}{
			"input not an Array".warn
		}
	}

	loadDefs {|synthDefName, cond|

			SynthDef(\fm1, {| outBus=0, t_trig=0, freq=1040, dcy=0.2, amp=0.3|
				var env = EnvGen.ar(Env.perc(0.01, freq.reciprocal * 200 * dcy ), t_trig, doneAction:0);
				OffsetOut.ar(outBus, PMOsc.ar(freq, freq*0.5, 2*TExpRand.kr(0.5, 4, t_trig),[0, SinOsc.kr(0.3,0,pi)],env) * amp);
			}).send(server);

			SynthDef(\fm2, {| outBus=0, t_trig=0, freq=1040, dcy=0.09, amp=0.2|
				var snd, env = EnvGen.ar(Env.perc(0.01,dcy*LFNoise2.kr(0.1,0.1,1)), t_trig, doneAction:0);
				snd = RLPF.ar(PMOsc.ar(freq, freq*[1.5, 2, 1].choose, TRand.kr(0.5, 3, t_trig),0), SinOsc.kr([0.1,0.11]).range(400, 1900), 0.4);
				OffsetOut.ar(outBus, snd * env * amp);
			}).send(server);
		
			SynthDef(\anasynth1, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, adcy=0.3, pdcy=0.07, ffreq=700, rq=0.3, amp=0.2|
				var aenv, penv, osc1, osc2, filtd;
				aenv = EnvGen.ar(Env.perc(0.01, rrand(0.02,adcy)), t_trig, doneAction: 0);
				penv = 1 + EnvGen.ar(Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
				pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
				sfreq = freq;
				pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1);
				osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
				osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
				filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
				OffsetOut.ar(0, filtd * aenv * amp * 0.8)
			}).send(server);	

			SynthDef(\anasynth2, {|t_trig=1, freq=100, sfreq=100, pfreq, pwidth=0.2, adcy=0.3, pdcy=0.07, ffreq=700, rq=0.3, amp=0.14|
				var aenv, penv, osc1, osc2, filtd;
				aenv = EnvGen.ar(Env.perc(0.01, rrand(0.02,adcy)), t_trig, doneAction: 0);
				penv = 1 + EnvGen.ar(Env.perc(0.01, pdcy), t_trig, timeScale:0.2, levelScale:-0.1, doneAction: 0);
				pwidth = pwidth * SinOsc.kr(0.1, [0, pi], 0.24, 0.25);
				sfreq = freq  * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
				pfreq = freq * SinOsc.kr([0.5, 1.333],0,0.01,1) * TChoose.kr(t_trig, [1.5, 1.0, 0.5, 2]);
				osc1 = VarSaw.ar(sfreq*penv,0,pwidth);
				osc2 = LFPulse.ar((pfreq?sfreq)*penv,0,pwidth);
				filtd = RLPF.ar(Mix([osc1, osc2]), ffreq, rq);
				OffsetOut.ar(0, filtd * aenv * amp * 0.8)
			}).send(server);
	}

}