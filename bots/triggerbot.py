#!/usr/bin/env python
# minibot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340, threading
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# minibot config

channel = "#mode+v"
nickname = "testbot"
server = "irc.gosub10.org"
IRCport = 6667
OSCport = 57120
mybotsynth = "/synth"

# Timer

class clock(threading.Thread):
    """Call a function after a specified number of seconds:

    t = Timer(30.0, f, args=[], kwargs={})
    t.start()
    t.cancel() # stop the timer's action if it's still waiting
    """

    def __init__(self):
        Thread.__init__(self)
        self.finished = Event()
        self.counter = 0

    def run(self):
        #self.finished.wait(self.interval)
        #if not self.finished.is_set():
        #    self.function(*self.args, **self.kwargs)
        #self.finished.set()
        self.counter = self.counter + 1


# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        self.count = clock(self.trigger)
#self.counter = 0
#self.t = threading.Timer(5.0, self.trigger)
        
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

#    def trigger(self):
#        c = self.connection
#        c.notice(self.channel, "beep")
#        self.t = threading.Timer(5.0, self.trigger)

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
        self.counter = self.counter + 1
        print self.counter

        if cmd:
            self.counter = 0
            #self.t.finished.set()
            #self.t.finished.clear()
            #self.t.run()
            

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)


# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
