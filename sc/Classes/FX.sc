ATape	{
/*
{ ATape.ar(AudioIn.ar(1), SinOsc.kr(0.1,0,0.07,0.1), 0.7) }.play;
*/
	*ar	{ |in, delay=0.25, amp=0.5|
		var local,iamp;
		iamp = Amplitude.kr(in);
		in = in * (iamp > 0.02); // noise gate
		local = LocalIn.ar(2);
		local = OnePole.ar(local, 0.4);
		local = OnePole.ar(local, -0.08);
		local = Rotate2.ar(local[0], local[1], 0.2);
		local = DelayN.ar(local, 2, delay);
		local = LeakDC.ar(local);	
		local = ((local + in) * 1.25).softclip;
		LocalOut.ar(local);
		^local * amp;
	}
	
}



AVerb	{
/*
{ AVerb.ar(AudioIn.ar(1), SinOsc.kr(0.1,0,0.7,0.8)) }.play;
*/	
	*ar { arg in, wet=0.1,nc=7, na=4;
		var a,c,z,y;
		c = nc; 			// combs
		a = na; 			// allpass
		//in=In.ar(0,1);	// 1 channel in
		
		z = DelayN.ar(in, 0.048,0.048); 		// predelay time 
		// 	y = Mix.arFill(c,{CombL.ar(z,0.1,rrand(0.01, 0.1),5)}); // static dlytimes
		//	delaytime modulation
		y = Mix.arFill(c,{CombL.ar(z,0.1,	LFNoise1.kr(0.1.rand, 0.04, 0.05),5)});
	    	// 4 allpass delays 
		a.do({ y = AllpassN.ar(y, 0.051, [rrand(0.01, 0.05),rrand(0.01, 0.05)], 1) });
		^Limiter.ar((in*(1-wet))+(wet*y),0.28)
	
	}
	
}


ATank	{
/*
{ ATank.ar(AudioIn.ar([1,2]),0.8) }.play;
*/
	*ar	{ arg in, inAmp=0.01;
	var local, amp;
	amp = Amplitude.kr(Mix.ar(in)*inAmp);
	in = in * (amp > 0.04) * 0.4; // noise gate		
//	local = LocalIn.ar(2) * MouseY.kr(0,1);
//	local = OnePole.ar(local, MouseX.kr(0,1));
	local = LocalIn.ar(2) * LFNoise1.kr(1,0.49,0.5);
	local = OnePole.ar(local, LFNoise1.kr(1,0.49,0.5));
	
	local = Rotate2.ar(local[0], local[1], 0.237);
	local = AllpassN.ar(local, 0.05, {Rand(0.01,0.05)}.dup, 2);
	local = DelayN.ar(local, 0.3, {Rand(0.15,0.33)}.dup);
	local = AllpassN.ar(local, 0.05, {Rand(0.03,0.15)}.dup, 2);
	local = LeakDC.ar(local);	
	local = local + in;

	4.do { var t;
		t = {Rand(0.005,0.02)}.dup;
		local = AllpassN.ar(local, t, t, 1);
	};
	
	LocalOut.ar(local);
	
	^local
	}
}


BufDelay {
	//your buffer should be the same numChannels as your inputs
	*ar { arg  bufnum=0,  inputs, delayTime, feedback=0.7, rate=1.0;
	
		var trig, delayedSignals;
		trig = Impulse.kr(delayTime.reciprocal);
		delayedSignals =  PlayBuf.ar(inputs.numChannels,bufnum,1.0,trig, 0, 0.0) * feedback + inputs;

		RecordBuf.ar(delayedSignals,bufnum,0.0,rate,0.0,1.0,0.0,trig);

		^delayedSignals
	}
}

Pan4Uur	{
	*ar	{ arg in, speed=0.1, cosPhase=0.5, level=1.0;
	
		^Pan4.ar(in, SinOsc.ar(speed), SinOsc.ar(speed,cosPhase*pi), level)
	
	}
}