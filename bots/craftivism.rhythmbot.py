#!/usr/bin/env python
# Originally written by Jonny Stutters
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make rhythmbot happy

import liblo, sys, r3340, random, re
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# rhythmbot config

channel = "#mode+v"
nickname = "rhythmbot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120

#mybotsynth = ["/bs3", "/bs3", "/bs4"]
mybotsynth = ["/bs6", "/bs2", "/bs6", "/bs3"]

if len(sys.argv) == 2:
    server = sys.argv[1]

# rhythmbot's guts

class rhythm(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        self.targetSynthId = 0
	
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            nick = nm_to_n(e.source())
            if nick == "animal":
                self.do_command(e, a[0])
        return
    
    def on_privmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
    
    def do_command(self, e, cmd):
        if re.match(nickname + " shut up", cmd):
            self.do_shutup()
        else:
            steps = len(cmd)
            pulses = self.count_vowels(cmd)
            beat = self.make_euclid(pulses, steps)
            print beat
            beatstring = ""
            i = 0
            for b in beat:
                if b == 1:
                    beatstring = beatstring + cmd[i]
                else:
                    beatstring = beatstring + "_"
                i += 1
            msg = liblo.Message(self.mybotsynth[self.targetSynthId])
            for i in beatstring:
                msg.add(ord(i))
            liblo.send(self.target, msg)
            c = self.connection
            c.notice(self.channel, "Animal's beat is " + beatstring)
            r3340.notify(1, 'new_beat:' + beatstring)
            self.targetSynthId += 1
            if self.targetSynthId >= len(self.mybotsynth):
                self.targetSynthId = 0

    def do_shutup(self):
        for i in range(0, len(self.mybotsynth)):    
            msg = liblo.Message(self.mybotsynth[i])
            msg.add(ord("_"))
            liblo.send(self.target, msg)
        c = self.connection
        c.notice(self.channel, "I'm shutting up")
        r3340.notify(1, "Shutting up")

    def count_vowels(self, word):
        v = 0
        for c in word:
            if c == "a" or c == "e" or c == "i" or c == "o" or c == "u":
                v += 1
        return v

    def make_euclid(self, pulses, steps):
        if pulses == 0:
            pulses = 1
        pauses = steps - pulses
        per_pulse = int(pauses / pulses)
        remainder = pauses % pulses
        rhythm = []
        for pulse in range(0, pulses):
            rhythm.append(1)
            for j in range(0, per_pulse):
                rhythm.append(0)
            if pulse < remainder:
                rhythm.append(0)
        return rhythm    

# rhythmbot go!

rhythmbot = rhythm(channel, nickname, server, IRCport, OSCport, mybotsynth)
rhythmbot.start()
