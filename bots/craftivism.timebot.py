#!/usr/bin/env python
# Originally written by Chris Hunt
# bits transplanted from minibot.py
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make timebot happy

import liblo, sys, r3340
import datetime, array, random
#import str
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# minibot config

channel = "#mode+v"
nickname = "timebot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
mybotsynth = "/bs3"
master_nick = "hungoverbot"
listen1 = "animal"
listen2 = "flea"
listen3	= "sting"
listen4 = "enya"
listen5 = "slayer"
listen6 = "bez"
commands = "!recall, !recall# !timewarp, !help. Type 'help ' before a command for help"
recall1 = "Nothing"
recall2 = "Nothing"
recall3 = "Nothing"
recall4 = "Nothing"
recall5 = "Nothing"
recall6 = "Nothing"

if len(sys.argv) == 2:
    server = sys.argv[1]

# minibot's guts

class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
        

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server
        c.notice(self.channel, "I am " + nickname + ". My master is " + master_nick + ", poke with a stick if I play up. I know how to " + commands + ". And I Listen to: " + listen1 + ", " + listen2 + ", " + listen3 + ", " + listen4 + ", " + listen5 + " and " + listen6 + ". It's an interesting Playlist, I admit.")
        self.recall1 = "Nothing"
        self.recall2 = "Nothing"
        self.recall3 = "Nothing"
        self.recall4 = "Nothing"
        self.recall5 = "Nothing"
        self.recall6 = "Nothing"

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
            
        #Inspired by WarGames
        if cmd == "play game":
            c.notice(self.channel, "Do you want to play a game," + nick + "?")
        
        #testing datetime and nested conditonals
        elif nick == master_nick and cmd == "lunchtime?":
            thistime = datetime.time.hour
            Lunch_start = 12
            Lunch_end = 14
            if thistime >= Lunch_start and thistime <= Lunch_end:
                   c.privmsg(self.channel, nick + ": Yes, it is lunch time ")
            elif thistime >= 14:
                   c.privmsg(self.channel, nick + ": No, Lunchtime has past. Is ended at " + str(Lunch_end) + ":00")
            elif thistime <= 13:
                   c.privmsg(self.channel, nick + ": No, Lunchtime will be here at " + str(Lunch_start) + ":00")       
            #else:
                #c.notice(self.channel, "No, it is not lunch time " + nick)
        
        #timebot commands
        elif (nick == listen1) or (nick == listen2) or (nick == listen3) or (nick == listen4) or (nick == listen5) or (nick == listen6):
            
            output = (nick + ": " + cmd)
            
            if cmd == "!recall":
                recalled =""
         
                if nick == listen1:
                    recalled = self.recall1
                elif nick == listen2:
                     recalled = self.recall2
                elif nick == listen3: 
                    recalled = self.recall3
                elif nick == listen4:
                      recalled = self.recall4
                elif nick == listen5:
                       recalled = self.recall5
                elif nick == listen6:
                       recalled = self.recall6
                
                c.notice(self.channel, nick + " last said: " + recalled)
                
                
            else:
                
                if nick == listen1:
                    self.recall1 = cmd
                elif nick == listen2:
                    self.recall2 = cmd
                elif nick == listen3: 
                    self.recall3 = cmd
                elif nick == listen4:
                    self.recall4 = cmd
                elif nick == listen5:
                    self.recall5 = cmd
                elif nick == listen6:
                    self.recall6 = cmd 
            
                print output
        
        
            
        elif cmd == "!recall1":
            c.notice(self.channel, listen1 + " last said: " + self.recall1)
        elif cmd == "!recall2":
            c.notice(self.channel, listen2 + " last said: " + self.recall2)
        elif cmd == "!recall3":
            c.notice(self.channel, listen3 + " last said: " + self.recall3)
        elif cmd == "!recall4":
            c.notice(self.channel, listen1 + " last said: " + self.recall4)
        elif cmd == "!recall5":
            c.notice(self.channel, listen1 + " last said: " + self.recall5)
        elif cmd == "!recall6":
            c.notice(self.channel, listen1 + " last said: " + self.recall6)
             
        elif cmd == "!timewarp":
            
            picked = random.randint(1, 6)
            pickedSynth = random.randint(1, 6)
            
            if picked == 1:
                randomNick = listen1
                recalled = self.recall1
            elif picked == 2:
                randomNick = listen2
                recalled = self.recall2
            elif picked == 3:
                randomNick = listen3
                recalled = self.recall3
            elif picked == 4:
                randomNick = listen4
                recalled = self.recall4
            elif picked == 5:
                randomNick = listen5
                recalled = self.recall5
            elif picked == 6:
                randomNick = listen6
                recalled = self.recall6
                

                        
            c.notice(self.channel, "I Picked - " + randomNick + " said  '" + recalled + "''and sent it to botsynth " + str(pickedSynth))
            
            if pickedSynth == 1:
                self.mybotsynth = "/bs1"
            elif pickedSynth == 2:
                self.mybotsynth = "/bs2"
            elif pickedSynth == 3:
                self.mybotsynth = "/bs3"
            elif pickedSynth == 4:
                self.mybotsynth = "/bs4"
            elif pickedSynth == 5:
                self.mybotsynth = "/bs5"
            elif pickedSynth == 6:
                self.mybotsynth = "/bs6"
                
            msg = liblo.Message(self.mybotsynth)
            for i in cmd:
                msg.add(ord(i))
            liblo.send(self.target, msg)
            r3340.notify(1, randomNick + ':' + recalled)
            
        elif cmd == "help !timewarp":
            c.notice(self.channel, "It's just a jump to the left,")
            c.notice(self.channel, "And then a step to the right.")
            c.notice(self.channel, "And then I Pick a random performer and tell you what they said and send it to a random synth")
                            
        elif cmd == "help !recall":
            c.notice(self.channel, "!recall prints out what you last said, if you are a member of the band")

        elif cmd == "!help":
            c.notice(self.channel, "I know commands such as " + commands)
            
        elif cmd == "help !help":
            c.notice(self.channel, "Do be so silly " + nick)
        
        elif cmd == "!recall#":
            c.notice(self.channel, "Replace the Hash with the Number of someone I'm listening to: 1 for " + listen1 + ", 2 for " + listen2 + ", 3 for " + listen3 + ", 4 for " + listen4 + ", 5 for " + listen5 + " and 6 for " + listen6 + ".")
                

        #elif cmd == "yo" and nick == "bob":
            #c.notice(self.channel, "I'm helpful, bob")

        # And fuck the rest!
        #else:
            #r3340.notify(0, nick + ':' + cmd)


# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
