/*
// 4 ways to reset, 1 is most brute, 4 the least
// 1. recompile to restart the whole, does 2 also
// make sure the osc port is still 57120), and the streamer needs to be restarted too
// emacs-sclang restart interpreter:
C-c C-l
// 2. evaluate this file to restart the botsynths, does 3 too
"~/Desktop/mode+v/utils/craftivism_conf/startBotSynths2.scd".loadPath;	
// 3. give the synths a kick in the but, reloading the initial settings, does 4 too
"~/Desktop/mode+v/utils/craftivism_conf/botsynths_settings.scd".loadPath;
// 4. light reset: just empty the patterns 
// there will still be something droish afterwards: no complete silence
Tdef(\clearAll).play


// clear patterns every hour
Tdef(\autoClearAll,{ loop{ Tdef(\clearAll).play; 3600.wait } }).play;


/////////////////////////////////////////////////////////////////////////

// make the synths; evaluate this file
"~/Desktop/mode+v/utils/craftivism_conf/startBotSynths2.scd".loadPath;
// set the synths' initial values
"~/Desktop/mode+v/utils/craftivism_conf/botsynths_settings.scd".loadPath;

// load automation
"~/Desktop/mode+v/utils/craftivism_conf/tdefs.scd".loadPath;

// start/stop automation
"~/Desktop/mode+v/utils/craftivism_conf/tdefs.scd".openTextFile;

// edit
"~/Desktop/mode+v/utils/craftivism_conf/botsynths_settings.scd".openTextFile;
"~/Desktop/mode+v/utils/craftivism_conf/tdefs.scd".openTextFile;

////////////////////////////////////////////////////////////////////////
*/

s.volume.volume_(-6); // master volume in dB

s = Server.local.waitForBoot({                                                                                                                                                                                                  
	{
	"~/Desktop/mode+v/utils/craftivism_conf/psynthdefs.scd".loadPath; // load the synthdefs
	"~/Desktop/mode+v/utils/craftivism_conf/adam2.scd".loadPath;

	if( a.notNil){ // free the old botsynths
		[a,b,c,d,e,f].do{|bs| bs.free };
		Post << nl << "****" << nl << "freed old botsynths" << nl << "****" << nl;
	};
	Post << nl << "****" << nl << "synthdefs loaded" << nl << "****" << nl; 1.wait;
	
	a = BotSynth2.new(s, botSynthName:"bs1", synthDefName:"drone1");                                                                                                                                         	
	b = BotSynth2.new(s, botSynthName:"bs2", synthDefName:"adamfm");
		//c = BotSynth2.new(s, botSynthName:"bs3", synthDefName:"ryanxxx");
	c = BotSynth2.new(s, botSynthName:"bs3", synthDefName:"ana1");
	d = BotSynth2Chord.new(s, botSynthName:"bs4", synthDefName:"chord");
		//e = BotSynth2.new(s, botSynthName:"bs5", synthDefName:"adamnoise");
	e = BotSynth2.new(s, botSynthName:"bs5", synthDefName:"ryanxxx");
	f = BotSynth2.new(s, botSynthName:"bs6", synthDefName:"drum2");
	g = BotSynth2.new(s, botSynthName:"bs7", synthDefName:"adamnoise");
	Post << nl << "****" << nl << "botsynths loaded" << nl << "****" << nl; 1.wait;
	
	BotSynth2.sync(true);
	Post << nl << "****" << nl << "botsynths synced" << nl << "****" << nl;1.wait;

    "~/Desktop/mode+v/utils/craftivism_conf/botsynths_settings.scd".loadPath;
   	Post << nl << "****" << nl << "settings applied" << nl << "****" << nl;

	}.r.play;

	// open the files in the editor
	"~/Desktop/mode+v/utils/craftivism_conf/psynthdefs.scd".openTextFile;
	"~/Desktop/mode+v/utils/craftivism_conf/adam2.scd".openTextFile;
	"~/Desktop/mode+v/utils/craftivism_conf/startBotSynths2.scd".openTextFile;
	"~/Desktop/mode+v/utils/craftivism_conf/tdefs.scd".openTextFile;
	"~/Desktop/mode+v/utils/craftivism_conf/botsynths_settings.scd".openTextFile;
	                                                                                                                                                                                                                 
});                                                                                                                                                                                                              
                                                                                                                                                                                                                 
                                                                                                                                                                                                               
/*

/bs3 xxx 5: [ 108, 100, 115, 107, 102 ]                                                                 
/bs6 adamfm 4: [ 35, 35, 35, 35 ]                                                                       
/bs4 chord 4: [ 107, 95, 95, 95 ]                                                                       
/bs3 xxx 3: [ 117, 105, 111 ]                                                                           
/bs3 xxx 6: [ 117, 95, 95, 115, 95, 95 ]                                                                
/bs4 chord 12: [ 117, 95, 105, 95, 111, 95, 111, 95, 100, 95, 100, 95 ]                                 
/bs3 xxx 14: [ 117, 95, 105, 95, 111, 95, 111, 95, 100, 95, 100, 95, 105, 105 ]                         
/bs2 adamfm 6: [ 97, 97, 97, 65, 65, 65 ]                                                               
/bs6 adamfm 20: [ 32, 32, 32, 32, 40, 40, 40, 40, 38, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 ]      
/bs2 adamfm 7: [ 46, 46, 46, 44, 44, 44, 44 ]        


/*
a.pattern = "...AAA...zzijjsdisjdzzzzzz............................4784.".ascii                                                                                                                                  
a.pattern = "______________________________________,.zzzddd".ascii                                                                                                                                               
                                                                                                                                                                                                                 
a.synth(\drone1);                                                                                                                                                                                                
a.synth(\drone2);                                                                                                                                                                                                
a.synth(\drone4);                                                                                                                                                                                                
a.synth(\drone9);                                                                                                                                                                                                
                       
a.sy.set(\amp, 0.25,\transpose, -2)                                                                                                                                                                              
a.synth(\drone)                                                                                                                                                                                                  
a.synth(\drum1)                                                                                                                                                                                                  
a.pattern = ".".ascii                                                                                                                                                                                            
                                                                                                                                                                                                                 
c.sy.set('amp', 0.02, \dcy, 0.073.rrand(0.4));                                                                                                                                                                   
c.transp = 0;                                                                                                                                                                                                    
c.transp = -36;                                                                                                                                                                                                  
c.synth(\xxx)                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                  
//set all                                                                                                                                                                                                    
// BotSynth2.botSynths.do{|bs| bs.postln; bs.sy.set(\amp, 0) }                                                                                                                                               
// reload synthdefs (after editting)                                                                                                                                                                         
"/home/lintian/Desktop/mode+v/utils/craftivism_conf/psynthdefs.scd".loadPath;                                                                                                                                    

b.pattern = "ABDF8abcd_____________".ascii                                                                                                                                                                       
c.pattern = "Kk09__ABVFff___0sjs#$,,,".ascii                                                                                                                                                                     
d.pattern = "a__________________g____________f___________".ascii                                                                                                                                                                            
e.pattern = "s8__DJ".ascii                                                                                                                                                                                    
f.pattern = "$*Esl_______".ascii                                                                                                                                                                                 
                                                                                                                                                                                                                 
*/

        /*                                                                                                                                                                                                       
        Tdef('autoSwitch',{                                                                                                                                                                                      
            var currSynth;                                                                                                                                                                                       
            10.wait;                                                                                                                                                                                             
            inf.do{ arg i;                                                                                                                                                                                       
                currSynth = ['adamnoise', 'adamfm'] @ j;                                                                                                                                                         
                j = i % 2;                                                                                                                                                                                       
                b.synth(currSynth);                                                                                                                                                                              
                60.wait;                                                                                                                                                                                         
            }                                                                                                                                                                                                    
        });                                                                                                                                                                                                      
                                                                                                                                                                                                                 
        Tdef('sync',{                                                                                                                                                                                            
            loop{                                                                                                                                                                                                
                BotSynth2.sync(true, 110);                                                                                                                                                                       
                1200.wait; // resync every 20 minutes                                                                                                                                                            
            }                                                                                                                                                                                                    
        }).play                                                                                                                                                                                                  
                                                                                                                                                                                                                 
        // Tdef('autoSwitch').play;                                                                                                                                                                              
                                                                                                                                                                                                                 
        a.startPyBot(botNickName:"m0n1", botSynthName:"bs1", commander1:"master", commander2:"super" , botPath:"monibot.py"); 1.wait;                                                                            
        b.startPyBot(botNickName:"m0n2", botSynthName:"bs2", commander1:"h4nk", commander2:"sufragette", botPath:"monibot.py"); 1.wait;                                                                          
        c.startPyBot(botNickName:"m0n3", botSynthName:"bs3", commander1:"shirley", commander2:"maclain", botPath:"monibot.py"); 1.wait;                                                                          
        d.startPyBot(botNickName:"w0dk4", botSynthName:"bs4", commander1:"sheik", commander2:"mohammad", botPath:"monibot.py"); 1.wait;                                                                          
    */                                                                                                                                                                                                           

                                                                                                                                                                                                         
a.dumpOSC(true,true);                                                                                                                                                                                            
a.pattern = "D________".ascii;                                                                                                                                                                                   
BotSynth.sync(true, 110);                                                                                                                                                                                        
//                                                                                                                                                                                                               
a.openChatWindow(nickname:"master")                                                                                                                                                                              
                                                                                                                                                                                                                 
// open up the python bot                                                                                                                                                                                        
"~/Desktop/mode+v/bots/monibot.py".openTextFile;                                                                                                                                                                 
                                                                                                                                                                                                                 
// stop the BotSynths                                                                                                                                                                                            
a.free; b.free; c.free; d.free;                                                                                                                                                                                  
                                                                                                                                                                                                                 
// stop python bots                                                                                                                                                                                              
("kill " ++ a.botPid).unixCmd;                                                                                                                                                                                   
("kill " ++ b.botPid).unixCmd;                                                                                                                                                                                   
("kill " ++ c.botPid).unixCmd;                  

*/
