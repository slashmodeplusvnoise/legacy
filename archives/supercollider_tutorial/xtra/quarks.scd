
preProcessor

/home/a/share/SuperCollider
/home/a/share/SuperCollider/Extensions

Quarks

export LANG='' ;  /usr/bin/svn co https://quarks.svn.sourceforge.net/svnroot/quarks/DIRECTORY /home/a/share/SuperCollider/quarks/DIRECTORY 2>&1

Quarks.checkoutDirectory
Quarks.updateDirectory

q = q ? ();
q.quarks = 

Quarks.install("PreProcessor");
Quarks.install("LinuxExternal");

Quarks.install("JACK");
Quarks.install("OSCClocks");

Quarks.install("ProcessTools");

Quarks.install("FreeSound");
Quarks.install("DayTimer");
Quarks.install("NetLib");
Quarks.install("recordmydesktop");
Quarks.install("batchNRT");


Quarks.uninstall("testquark");

// list those installed
Quarks.installed

Quarks.repos.quarks[27]

E.menubar(1)


Quark: AmbIEM [1]
Quark: AudioUnitBuilder [0.5]
Quark: Automation
Quark: CommonTests
Quark: Conductor [1]
Quark: ContextSnake
Quark: CrucialTests
Quark: Ctk
Quark: SenseWorld DataNetwork [0.3]
Quark: DayTimer
Quark: Dimple [0.1]
Quark: FileListView
Quark: FileLog [0.1]
Quark: FreeSound [1]
Quark: GD_ToolboxWindow
Quark: GLOBOL
Quark: GNUPlot [0.3]
Quark: Generators [0.1]
Quark: GeoGraphy [0.2]
Quark: Hadron [1.5]
Quark: HadronPlugins [1]
Quark: HierSch [0.9]
Quark: Image
Quark: JACK [0.2]
Quark: JInT
Quark: KDTree
Quark: Knob
Quark: KrToggleEditor
Quark: Laptop [0.1]
Quark: LinuxExternal [0.2]
Quark: MP3 [1.1]
Quark: MagicPreset [1]
Quark: MathLib
Quark: McldUnitTests
Quark: MovieMarkers [0.1]
Quark: NetLib [0.2]
Quark: OSCClocks [0.1]
Quark: OpenObject [0.2]
Quark: Orb [0.2]
Quark: PitchCircle [0.8]
Quark: Plfnoise
Quark: PopUpTreeMenu
Quark: PreProcessor
Quark: ProcessTools [1]
Quark: ProteinBioSynthesis
Quark: RecordMyDesktop [0.1]
Quark: Republic [0.1]
Quark: scpodcast [1]
Quark: SCPyduino [0.11]
Quark: SETO [1]
Quark: SNBox
Quark: SenseWorld [0.1]
Quark: SpeakersCorner
Quark: Spectrogram [1]
Quark: SynthDefPool
Quark: TUIO [1]
Quark: TabbedView [1.26]
Quark: Tendency
Quark: TriggerView
Quark: Turtle
Quark: UGenPatterns
Quark: UGenStructure
Quark: UnitTesting
Quark: VuView
Quark: WarpExt
Quark: Wavesets
Quark: XML [0.21]
Quark: Arduino
Quark: batchNRT [1.1]
Quark: cxaudio
Quark: cxpatterns
Quark: ddwChucklib [1]
Quark: ddwCommon [1]
Quark: ddwEQ [1]
Quark: ddwGUIEnhancements [1]
Quark: ddwGUIEnhancementsForSwingOSC [1]
Quark: ddwMIDI [1]
Quark: ddwMixerChannel [1]
Quark: ddwPatterns [1]
Quark: ddwPeakMonitor [1]
Quark: ddwPrototype [1]
Quark: ddwSensitivity [1]
Quark: ddwStatusBox [1]
Quark: ddwTemperament [1]
Quark: ddwTimeline [1]
Quark: ddwVoicer [1]
Quark: dewdrop_lib [1]
Quark: DMX [0.1]
Quark: heatMap [1]
Quark: ixi [4]
Quark: ixiViews [1]
Quark: lfsaw.de [1.1]
Quark: livecoding
Quark: rd_ctl
Quark: rd_dot
Quark: redDefault
Quark: redMst
Quark: redSampler
Quark: redUniverse
Quark: scgraph
Quark: testquark
Quark: wslib [0.31]


q.quarks[0]


Quarks.checkout("testquark");

Quarks.install("testquark");
