// w3m sc-help viewer. after you opened the helpfile do    
// M-x sclang-mode in order to evaluate code        
C-c C-h (with cursor on the classname)    
// try it on
SinOsc EnvGen UGen E


// or read it in another browser
UGen.helpFilePath;


// print out all subclasses of UGen
UGen.dumpClassSubtree


//-- class (method) lookup
C-c : // definitions
.ar   // everything responding to .ar can play at audio rate, is a UGen    


// 'sclang land' class lookup: 
EmacsClassBrowser(EnvGen) // lookup
// press Enter on the *ar method, 
// you see its arguments:    
// *ar { arg envelope, gate = 1.0, levelScale = 1.0, levelBias = 0.0, timeScale = 1.0, doneAction = 0;     




