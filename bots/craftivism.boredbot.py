#!/usr/bin/env python
# boredbot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make boredbot happy

import liblo, sys, r3340, time, random
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr

# boredbot config

channel = "#mode+v"
nickname = "boredbot"
server = "irc.gosub10.org"
IRCport = 6666
OSCport = 57120
targets = ["melodybot", "rhythmbot", "fleabot", "enyaBot"]
mybotsynth = ""

if len(sys.argv) == 2:
    server = sys.argv[1]

# boredbot's guts

class bored(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport
	
        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server
        targetId = 0
        while 1:
            c = self.connection
            waittime = random.randint(30, 180)
            c.privmsg(self.channel, "mmm... let's see...")
            roulette = random.randint(0,5)
            
            if roulette == 0:

                seq = ""
                for i in range(random.randint(2,8)):
                    seq += chr(random.randint(0,255))
            
                c.privmsg(targets[targetId], seq)
                time.sleep(10)
                c.privmsg(self.channel, "Cool huh?  I'll be back ....")
                targetId += 1
                if targetId >= len(targets):
                    targetId = 0
                time.sleep(waittime)

            elif roulette == 1:

                c.privmsg(self.channel, "!timewarp")
                time.sleep(10)
                c.privmsg(self.channel, "awesome")
                time.sleep(waittime)

            else:
                time.sleep(waittime)
            
# boredbot go!

boredbot = bored(channel, nickname, server, IRCport, OSCport, mybotsynth)
boredbot.start()
