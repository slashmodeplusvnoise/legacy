// ------------ UGen list sorted by type

Analysis

Amplitude
Compander
Pitch
Slope
ZeroCrossing

Control

DegreeToKey
AmpComp
Slew
MouseX
MouseY
MouseButton

Input/Output

AudioIn
In
InTrig
InFeedback
Out
ReplaceOut
XOut
OffsetOut
LocalIn
LocalOut
SharedIn
SharedOut

Oscillators

Blip
Formant
FSinOsc
Gendy1
Gendy2
Gendy3
Impulse
Klang
LFCub
LFPar
LFPulse
LFSaw
Osc
OscN
Pulse
Saw
SinOsc
SyncSaw
VarSaw
VOsc
VOsc3

Delays

AllpassC
AllpassL
AllpassN
BufAllpassC
BufAllpassL
BufAllpassN
BufDelayC
BufDelayL
BufDelayN
BufCombC
BufCombL
BufCombN
CombC
CombL
CombN
Delay1
Delay2
DelayC
DelayL
DelayN
PingPong
PitchShift

Multiple Channels

Mix
MultiOutUGen
OutputProxy
BiPanB2
DecodeB2
LinPan2
LinXFade2
Pan2
Pan4
PanAz
PanB
PanB2
Rotate2
XFade2

Physical Models

Ball
Spring
TBall

Filters

BPF
BPZ2
BRF
Formlet
FOS
HPF
HPZ1
HPZ2
Integrator
Klank
Lag
Lag2
Lag3
LeakDC
Limiter
LinExp
LinLin
LPF
LPZ1
LPZ2
Median
Normalizer
OnePole
OneZero
Resonz
RHPF
Ringz
RLPF
SOS
TwoPole
TwoZero

Noise

LFNoise1
LFNoise2 
LFNoise0 
LFClipNoise      
//NoahNoise        
WhiteNoise
GrayNoise 
Hasher
MantissaMask
//PinkerNoise
PinkNoise
ClipNoise        
Dust             
Dust2           
//Latoocarfian     
//Rossler
Crackle               

Triggers

MostChange       
PulseCount
Stepper
Gate
CoinGate             
Peak             
PulseDivider
LastValue
Latch
Trig
Trig1
SendTrig
Sweep
Timer
Phasor


Frequency Domain

FFT
Convolution
PV_ConformalMap

BufferManipulation

BufRd
BufWr
DiskIn
DiskOut
Index
WrapIndex
PlayBuf
RecordBuf
PingPong
TGrains

Binary Operators

+
-
*
/
**
absdif
amclip
atan2
clip2
difsqr
excess
fold2
hypot
hypotApx
max
min
ring1
ring2
ring3
ring4
round
scaleneg
sqrdif
sqrsum
sumsqr
thresh
trunc
wrap2


InfoUGens

BufChannels
BufDur
BufFrames
BufRateScale
BufSampleRate
NumRunningSynths
SampleDur
SampleRate

SynthControlling-UGens

Control
DetectSilence
EnvGen
Line
XLine
Linen
Free
FreeSelf
Pause
PauseSelf


Random Values

Rand
ExpRand          
LinRand  
NRand
IRand 
TIRand
TRand
TExpRand
RandSeed
RandID

ArrayUsage

Select 
TWindex
