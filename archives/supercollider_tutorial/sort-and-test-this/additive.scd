(~path +/+ "home.scd").openTextFile; // go back to main menu
///

s.boot;

SynthDef(\addi1,{|t_trig=0, freq=200, lvls=#[0.5,0, 0.8, 0.1, 0,0,0,0, 0,0,0,0, 0,0,0,0], amp=0.1|
	var snd=0, snds;
	snds = lvls.collect{|lvl, index|
		SinOsc.ar(freq.lag(0.2)*(index+1),pi.rand,lvl)
	};
	snd = Mix(snds);
	Out.ar(0, Pan2.ar(snd, LFNoise1.kr( LFPar.kr(0.2,0.3,5,8)), amp))
}).send(s)

x.free;
x = Synth(\addi1)
x.set(\lvls, [0.8,0.5,0.1, 0.3, 0.1, 0.1, 0.04])

x.set(\freq, 150)
x.set(\lvls, 1 / Array.series(8, 1, 2) )
x.set(\lvls, (1 / Array.geom(16, 1, 1.4)).post )

Tdef(\radd, { loop{ x.set(\lvls, 0.3 * (1 / Array.rand(16, 1, 16)).round(0.01).postcln ); [0.25, 1, 2, 5].choose.wait} }).play

Tdef(\radd).stop

x.free;
