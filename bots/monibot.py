#!/usr/bin/env python
# minibot.py
# A small and cute bot easy to hack and cuddle
# DEPENDS: python-liblo, python-irclib, r3340


# modules required to make minibot happy

import liblo, sys, r3340
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr


#### minibot config

nickname = "m0n1" 		# this bot's nickname
server = "irc.goto10.org"
IRCport = 6667
channel = "#mode+v"

OSCport = 57120 		# sc's receiving port ( NetAddr.langPort )
mybotsynth = "bs1" 		# sc botsynth's osctag

				# this bot listens to:
commander1 = "h4nk"
commander2 = "shirley69"

if len(sys.argv) == 6:
    (nickname, mybotsynth, OSCport, commander1, commander2) = (sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
else:
    if len(sys.argv) == 4:
        (nickname, mybotsynth, OSCport) = (sys.argv[1], sys.argv[2], sys.argv[3]) # use defaults for commander1 and 2
    else:
        print '\033[1;31mUsage:\033[m minibot.py <nickname> <botSynthName> <OSC port> <commander1> <commander2>'
        print '\033[1;31mexample:\033[m minibot.py mbot1 abc 57121'
        print '\033[1;31musing:\033[m ' + nickname + ' ' + mybotsynth + ' ' + str(OSCport) + ' ' + commander1 + ' ' + commander2

mybotsynth = '/' + mybotsynth


# minibot's guts
class mini(SingleServerIRCBot):
    def __init__(self, channel, nickname, server, IRCport, OSCport, mybotsynth):
        SingleServerIRCBot.__init__(self, [(server, IRCport)], nickname, nickname)
        self.mybotsynth = mybotsynth
        self.server = server
        self.channel = channel
        self.OSCport = OSCport

        try:
            self.target = liblo.Address(self.OSCport)
        except liblo.AddressError, err:
            print str(err)
            sys.exit("oioioi OSC address error")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)
        print "connected to: " + self.server

    def on_pubmsg(self, c, e):
        a = e.arguments()[0].split(":", 1)
        if len(a) == 1 :
            self.do_command(e, a[0])
        return

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection

        # commander1 gets his message sent to OSC directly...
        if nick == commander1:
            msg = liblo.Message(self.mybotsynth)
            #msg = '/' + msg
            for i in cmd:
                    msg.add(ord(i))
            liblo.send(self.target, msg)
            r3340.notify(1, nick + ':' + cmd)
   
        # ...but commander2 messages are scried first.
        elif nick == commander2:
            msg = liblo.Message(self.mybotsynth)
            
            string = r3340.cervelle(cmd)
            string.scry(100)

            for i in string.message:
                    msg.add(ord(i))
            liblo.send(self.target, msg)

            r3340.notify(1, nick + ':' + string.message)

        # And fuck the rest!
        else:
            r3340.notify(0, nick + ':' + cmd)


# minibot go!

minibot = mini(channel, nickname, server, IRCport, OSCport, mybotsynth)
minibot.start()
