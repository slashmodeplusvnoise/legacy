
UGen
[
  PV_Whiten
  Line
  IEnvGen
  ListTrig
  Pokey
  AbstractOut
  [
    LocalOut
    XOut
    SharedOut
    Out
      [ OffsetOut ReplaceOut ]
  ]
  Logistic
  LinRand
  MembraneHexagon
  KeyState
  LFSaw
    [ LFTri LFCub LFPar ]
  FreeVerb
  PitchShift
  LTI
  BitAnd
    [ BitOr BitXor ]
  ExpRand
  PV_XFade
  FFTTriggered
  RandID
  Nes2
  AtsUGen
    [ AtsNoiSynth AtsNoise AtsSynth AtsAmp AtsFreq ]
  WeaklyNonlinear
  Compander
  VOsc3
  PSinGrain
  Pause
  FreeSelf
  MoogVCF
  NL
  GravityGrid
  SpecPcile
  ScopeOut
  SinOsc
  FFTComplexDev
  ToggleFF
  SinOscFB
  PrintVal
  Convolution2
  Balance
  MaxLocalBufs
  GaussTrig
  Pulse
  FFTPhaseDev
  XFade
    [ XFade2 LinXFade2 ]
  Gendy2
  PulseCount
  [
    SetResetFF
    Peak
      [ RunningMin RunningMax ]
  ]
  Sweep
  InfoUGenBase
  [
    RadiansPerSample
    NumRunningSynths
    SampleDur
    NumBuffers
    ControlDur
    ControlRate
    NumInputBuses
    NumControlBuses
    SubsampleOffset
    NumAudioBuses
    SampleRate
    NumOutputBuses
  ]
  SendTrig
    [ SendReply ]
  LastValue
  PV_CommonMag
    [ PV_CommonMul ]
  FreqShift
  CoinGate
  FFTFluxPos
  SID6581f
  Dbrown2
  CompanderD
  MulAdd
  FFTInterleave
  MoogLadder
  FitzHughNagumo
  RedNoise
  CheckBadValues
  Hasher
  Astrocade
  BufInfoUGenBase
  [
    BufChannels
    BufFrames
    BufDur
    BufSamples
    BufSampleRate
    BufRateScale
  ]
  Latoocarfian2DN
    [ Latoocarfian2DC Latoocarfian2DL ]
  TExpRand
  Free
  MCLDChaosGen
    [ ChuaL RosslerResL ]
  LFPulse
  ZeroCrossing
  RecPBufDelayL
  PosRatio
  Slub
  StandardTrig
  SelectL
  TBetaRand
  VarSaw
  DynKlank
  HenonTrig
  MouseButton
  Pluck
  LinExp
  GravityGrid2
  PV_MagGate
  Latch
    [ Gate ]
  Dust2
  MantissaMask
  Dust
  ClearBuf
  LorenzTrig
  BeepU
  Onsets
  WeaklyNonlinear2
  Tap
  K2A
    [ T2A ]
  NRand
  JScopeOut
  PeakFollower
  FFTSlope
  LP_Synth
  PVSynth
  DegreeToKey
  MZPokey
  SetBuf
  LPCSynth
  PV_Morph
  MungerMono
  InRect
  AmpComp
    [ AmpCompA ]
  TermanWang
  LPCAnal
  PV_MagLog
  EnvGen
  TBrownRand
  Select
  Index
    [ Shaper DetectIndex WrapIndex IndexInBetween ]
  AmplitudeMod
  PulseCount3
  BFDecoder
    [ BFDecode1 FMHDecode1 ]
  FFTSpread
  Maxamp
  Streson
  Instruction
  Logger
  RunningSum
  OscN
  IIRFilter
  PV_Compander
  BufWr
  Rand
  FFTRumble
  PauseSelf
  LPCSynth2
  BasicOpUGen
    [ BinaryOpUGen UnaryOpUGen ]
  BitRightShift
  Linen
  PV_MagScale
  COsc
  ImpulseDrop
  JoshGrain
  [
    SinGrain
    InGrain
    FMGrainB
    FMGrainI
    InGrainI
    SinGrainB
    FMGrain
    InGrainB
    SinGrainI
    BufGrain
    MonoGrain
    BufGrainI
    BufGrainB
  ]
  Concat
  FFTCrest
  PV_MagExp
  Duty
    [ TDuty ]
  WhiteNoise
    [ PinkNoise BrownNoise ClipNoise GrayNoise ]
  SVF
  Squiz
  SpecFlatness
  TBall
  FFTMKL
  MostChange
    [ LeastChange ]
  Beep
  FFTPower
  WaveTerrain
  LFNoise0
  [
    LFClipNoise
    LFDNoise3
    LFDNoise0
    LFNoise2
    LFDNoise1
    LFNoise1
    LFDClipNoise
  ]
  LatoocarfianTrig
  DUGen
  [
    Dswitch1
      [ Dswitch ]
    Dseries
    Dpoll
    DbufTag
      [ Dtag ]
    Dbufwr
    Dbrown
      [ Dibrown ]
    ListDUGen
      [ Dshuf Dxrand Drand Dseq Dser ]
    Dgeom
    Dstutter
    Dwhite
      [ Diwhite ]
    Donce
    Dfsm
    Dbufrd
  ]
  Breakcore
  LPC_Synth
  BitLeftShift
    [ BitUnsignedRightShift ]
  Gendy3
  NestedAllpassN [ NestedAllpassC NestedAllpassL ]
  Getenv
  FhnTrig
  KmeansToBPSet1
  BufDelayN
    [ BufDelayC BufDelayL ]
  Crackle
  Spring
  IFFT
  Henon2DN
    [ Henon2DC Henon2DL ]
  Ball
  Osc
  Delay1
    [ Delay2 ]
  Filter
  [
    BPF
      [ BRF ]
    LPF
      [ HPF ]
    InsideOut
    LPZ2
      [ HPZ2 BPZ2 BRZ2 ]
    LagUD
      [ Lag2UD Lag3UD ]
    Decay2
    Decay
    OnePole
      [ OneZero ]
    Resonz
    Friction
    MoogFF
    Median
    Integrator
    DetectSilence
    Lag
      [ Ramp Lag3 Lag2 ]
    LPZ1
      [ HPZ1 ]
    Ringz
    WaveLoss
    FOS
    RLPF
      [ RHPF ]
    MeanTriggered
    MedianTriggered
    MidEQ
    Slew
    LeakDC
    TwoPole
      [ TwoZero APF ]
    BEQSuite
    [
      BBandStop
      BHiPass
      BLowShelf
      BPeakEQ
      BAllPass
      BHiShelf
      BBandPass
      BLowPass
    ]
    Slope
    Formlet
    SOS
  ]
  KeyTrack
  PV_DiffMags
  SpecCentroid
  FFTDiffMags
  BitNot
  HilbertFIR
  Loudness
  Standard2DN
    [ Standard2DC Standard2DL ]
  LPCError
  MembraneCircle
  FFTFlatness
  DoubleWell
  XLine
  Vibrato
  AY
  Formant
  PV_MagMinus
  OnsetsDS
  TIRand
  DemandEnvGen
  A2K
    [ T2K ]
  LinLin
  TRand
  GbmanTrig
  BMoog
  PulseDivider
  PulseCount2
  FreeSelfWhenDone
  Klang
  RecordBuf
  IRand
  DoubleNestedAllpassN
    [ DoubleNestedAllpassL DoubleNestedAllpassC ]
  Lorenz2DN
    [ Lorenz2DL Lorenz2DC ]
  Fhn2DN
    [ Fhn2DC Fhn2DL ]
  Normalizer
    [ Limiter ]
  DoubleWell3
  Dgauss
  AudioMSG
  Blip
  PauseSelfWhenDone
  PartConv
  SN76489
  Gendy1
  OutputProxy
  SortBuf
  Done
  Saw
  Phasor
  DynKlang
  CombN
    [ CombL CombC AllpassC AllpassN AllpassL ]
  PV_ChainUGen
  [
    PV_JensenAndersen
    PV_MagMap
    PV_MagFreeze
    FFT
    PV_BinBufRd
    PV_RectComb2
    PV_BrickWall
    PV_Freeze
    PV_PhaseShift
    PV_MagAbove
      [ PV_MagClip PV_MagBelow PV_LocalMax ]
    PV_BufRd
    Unpack1FFT
    PV_HainsworthFoote
    FFTTrigger
    PV_BinScramble
    PV_NoiseSynthP
      [ PV_PartialSynthF PV_PartialSynthP PV_NoiseSynthF ]
    PV_MagMul
    [
      PV_Max
      PV_Copy
      PV_CopyPhase
      PV_Mul
      PV_Add
      PV_Div
      PV_Min
    ]
    PV_MagDiv
    PV_MagSquared
      [ PV_PhaseShift270 PV_Conj PV_MagNoise PV_PhaseShift90 ]
    PV_PlayBuf
    PV_Diffuser
    PV_OddBin
      [ PV_EvenBin ]
    PV_MagSmear
    PV_BinWipe
    PV_BinPlayBuf
    PackFFT
    PV_MagBuffer
      [ PV_FreqBuffer ]
    PV_MaxMagN
      [ PV_MinMagN ]
    PV_RectComb
    PV_RandComb
    PV_RecordBuf
    PV_ConformalMap
    PV_Invert
    PV_RandWipe
    PV_BinShift
      [ PV_MagShift ]
    PV_BinDelay
  ]
  Klank
  VOsc
  Convolution2L
  BufCombN
    [ BufAllpassC BufCombL BufCombC BufAllpassN BufAllpassL ]
  NL2
  Stepper
  LocalBuf
  SinTone
  DiskOut
  Convolution3
  InRange
    [ Clip Fold Schmidt Wrap ]
  ListTrig2
  Trig1
    [ Trig TDelay ]
  NLFiltN
    [ NLFiltL NLFiltC ]
  FSinOsc
  Convolution
  Amplitude
  Timer
  DelayN
    [ DelayL DelayC ]
  RedPhasor
  Atari2600
  AnalyseEvents2
  Impulse
  FFTPercentile
  DoubleWell2
  Gbman2DN
    [ Gbman2DL Gbman2DC ]
  EnvDetect
  MouseX
    [ MouseY ]
  TWChoose
  Poll
  SyncSaw
  RLPFD
  MultiOutUGen
  [
    Panner
    [
      Rotate2
      RotateN
      XFadeRotate
      DecodeB2
      Pan4
      PanAz
      Balance2
      RotateL
      PanB2
      PanB
      BFPanner
      [
        A2B
        BFEncode1
        Rotate
        Tilt
        Tumble
        BFEncodeSter
        B2A
        UHJ2B
        BFManipulate
        FMHEncode2
        B2UHJ
        FMHEncode0
        FMHEncode1
        B2Ster
        BFEncode2
      ]
      Pan2
        [ LinPan2 ]
      BiPanB2
      JoshMultiChannelGrain
        [ MonoGrainBF ]
    ]
    RosslerL
    GrainIn
    BeatTrack
    AbstractIn
    [
      LocalIn
      SharedIn
      LagIn
      InFeedback
      InTrig
      In
    ]
    CQ_Diff
    Demand
    LPCVals
    AutoTrack
    Warp1
    GrainSin
    LP_Ana
    Pitch
    FM7
    FincoSprottL
    PlayBuf
    TGrains3
    AtsParInfo
    UnpackFFT
    LPCVals2
    FreeVerb2
    SOMRd
    RMAFoodChainL
    GrainFM
    Control
      [ LagControl TrigControl ]
    Munger
    AudioControl
    TGrains2
    TGrains
    FFTSubbandFlux
    Hilbert
    Tartini
    PVInfo
    FincoSprottS
    DrumTrack
    AY8910
    StereoConvolution2L
    FincoSprottM
    DC
    DiskIn
    GrainBuf
    LPC_Ana
    BufRd
    BinData
    VMScan2D
    MFCC
    GVerb
    SOMTrain
    LoopBuf
    Qitch
    FFTSubbandFlatness
    FFTSubbandPower
    Silent
    BeatTrack2
    FFTFlatnessSplitPercentile
    VDiskIn
    VEPConvolution
    JoshMultiOutGrain
    [
      FMGrainIBF
      BufGrainBF
      SinGrainBBF
      BufGrainIBF
      FMGrainBBF
      SinGrainIBF
      BFGrainPanner
        [ InGrainBBF InGrainBF InGrainIBF ]
      SinGrainBF
      FMGrainBF
      BufGrainBBF
    ]
  ]
  LFBrownNoise0
    [ LFBrownNoise1 LFBrownNoise2 ]
  ChaosGen
  [
    FBSineN
      [ FBSineC FBSineL ]
    LatoocarfianN
      [ LatoocarfianC LatoocarfianL ]
    QuadN
      [ QuadL QuadC ]
    HenonN
      [ HenonL HenonC ]
    LinCongN
      [ LinCongL LinCongC ]
    LorenzL
    StandardN
      [ StandardL ]
    GbmanN
      [ GbmanL ]
    CuspN
      [ CuspL ]
  ]
  RandSeed
  PV_SoftWipe
    [ PV_Cutoff ]
  TGaussRand
  Gendy4
    [ Gendy5 ]
  TWindex
  LFGauss
  FFTCentroid
  FFTFlux
  Max
]
UGen

E

E

ERROR: Message 'colums' not understood.
RECEIVER:
class E (08A44630) {
  instance variables [19]
    name : Symbol 'E'
    nextclass : class EZGui (0859D8B0)
    superclass : Symbol 'Object'
    subclasses : nil
    methods : nil
    instVarNames : nil
    classVarNames : nil
    iprototype : nil
    cprototype : nil
    constNames : nil
    constValues : nil
    instanceFormat : Integer 0
    instanceFlags : Integer 0
    classIndex : Integer 350
    classFlags : Integer 0
    maxSubclassIndex : Integer 350
    filenameSymbol : Symbol '/home/a/Desktop/mode+v/sc/E.sc'
    charPos : Integer 1749
    classVarIndex : Integer 274
}
ARGS:
   Integer 1
CALL STACK:
	DoesNotUnderstandError:reportError   B76B1000
		arg this = <instance of DoesNotUnderstandError>
	Nil:handleError   B7804830
		arg this = nil
		arg error = <instance of DoesNotUnderstandError>
	Thread:handleError   B78054F0
		arg this = <instance of Thread>
		arg error = <instance of DoesNotUnderstandError>
	Object:throw   B7802F60
		arg this = <instance of DoesNotUnderstandError>
	Object:doesNotUnderstand   B7806090
		arg this = class E
		arg selector = 'colums'
		arg args = [*1]
	Interpreter:interpretPrintCmdLine   B7797F80
		arg this = <instance of Interpreter>
		var res = nil
		var func = <instance of Function>
		var code = "E.menubar(0); E.lines(1); E...."
		var doc = <instance of ScelDocument>
	Process:interpretPrintCmdLine   B7803DA0
		arg this = <instance of Main>

/home/a/Desktop/mode+v/workshop/supercollider_tutorial



ERROR: Message 'openTextFile' not understood.
RECEIVER:
   nil
ARGS:
CALL STACK:
	DoesNotUnderstandError:reportError   B76CF1A0
		arg this = <instance of DoesNotUnderstandError>
	Nil:handleError   B76CC980
		arg this = nil
		arg error = <instance of DoesNotUnderstandError>
	Thread:handleError   B76CB5E0
		arg this = <instance of Thread>
		arg error = <instance of DoesNotUnderstandError>
	Object:throw   B76CE160
		arg this = <instance of DoesNotUnderstandError>
	Object:doesNotUnderstand   B76CF140
		arg this = nil
		arg selector = 'openTextFile'
		arg args = [*0]
	Interpreter:interpretPrintCmdLine   B77474C0
		arg this = <instance of Interpreter>
		var res = nil
		var func = <instance of Function>
		var code = "a.openTextFile;"
		var doc = <instance of ScelDocument>
	Process:interpretPrintCmdLine   B76CE540
		arg this = <instance of Main>

9957
RESULT = 0

ERROR: Message 'openTextFile' not understood.
RECEIVER:
   nil
ARGS:
CALL STACK:
	DoesNotUnderstandError:reportError   09017D60
		arg this = <instance of DoesNotUnderstandError>
	Nil:handleError   09019BC0
		arg this = nil
		arg error = <instance of DoesNotUnderstandError>
	Thread:handleError   09015270
		arg this = <instance of Thread>
		arg error = <instance of DoesNotUnderstandError>
	Object:throw   090160E0
		arg this = <instance of DoesNotUnderstandError>
	Object:doesNotUnderstand   09018A50
		arg this = nil
		arg selector = 'openTextFile'
		arg args = [*0]
	Interpreter:interpretPrintCmdLine   08E97F40
		arg this = <instance of Interpreter>
		var res = nil
		var func = <instance of Function>
		var code = "a.openTextFile;"
		var doc = <instance of ScelDocument>
	Process:interpretPrintCmdLine   090159F0
		arg this = <instance of Main>

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/get_help.scd

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/scel1.scd

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/.xtra/emacs-lisp.scd

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/day2.scd

/home/a/Desktop/mode+v/workshop/supercollider_tutorial

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/config.scd

E

E

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/get_help.scd

/home/a/Desktop/mode+v/workshop/supercollider_tutorial/.xtra/emacs-default-keycommands.scd
