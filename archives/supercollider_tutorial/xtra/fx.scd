(~path +/+ "home.scd").openTextFile;
////

//~fxGroup = Group.new

(
SynthDef(\flanger,{|dlyTime=0.002, fback=1, mix=0.5, amp=0.9|
	var dry, wet, dlyMod;
	dry = In.ar(0,2);
	dlyMod = 0.001;
	wet = dry;
	4.do{|i|
		wet = wet + CombC.ar(dry, 0.5, dlyMod * (i+1) * SinOsc.kr(i*0.025 + 0.1.rand, pi.rand, 0.5,1), 0.1* fback);
	};
	ReplaceOut.ar(0, ((wet*mix) + dry*(1-mix)) * amp)
}).send(s);

SynthDef(\echo,{|dlyTime=0.32, fback=3, mix=0.5, amp=0.9|
	var dry, wet;
	dry = In.ar(0,2);
	wet = CombN.ar(dry, 3, dlyTime, dlyTime*fback);
	ReplaceOut.ar(0, ((wet*mix) + dry*(1-mix)) * amp * 2)
}).send(s);

SynthDef(\bitRound,{|round=0.01, amp=0.9|
	var dry, wet;
	dry = In.ar(0,2);
	wet = dry.round(SinOsc.kr(0.1,0,round).abs);
	ReplaceOut.ar(0, wet * amp)
}).send(s);

SynthDef(\distort,{|gain=1, amp=0.9|
	var dry, wet;
	dry = In.ar(0,2);
	wet = (dry*gain).softclip;
	ReplaceOut.ar(0, wet * amp * gain.reciprocal)
}).send(s);

SynthDef(\verb, { arg wet=0.1, nc=7, na=4;
		var a,c,z,y, in;
		c = nc; 			// combs
		a = na; 			// allpass
		in=In.ar(0,2);
	
		z = DelayN.ar(in, 0.048,0.048); 		// predelay time 
		//	delaytime modulation
		y = Mix.arFill(c,{CombL.ar(z,0.1,	LFNoise1.kr(0.1.rand, 0.04, 0.05),5)});
	    	// 4 allpass delays 
		a.do({ y = AllpassN.ar(y, 0.051, [rrand(0.01, 0.05),rrand(0.01, 0.05)], 1) });
		Out.ar(0, Limiter.ar((in*(1-wet))+(wet*y),0.28))

	}).send(s);

SynthDef(\tapedelay, { arg inAmp=0.1;
	var local, amp, in;
	in = In.ar(0, 2) * (inAmp > 0.04) * 0.4; // noise gate		
	amp = Amplitude.kr(Mix.ar(in)*inAmp);
//	local = LocalIn.ar(2) * MouseY.kr(0,1);
//	local = OnePole.ar(local, MouseX.kr(0,1));
	local = LocalIn.ar(2) * LFNoise1.kr(1,0.49,0.5);
	local = OnePole.ar(local, LFNoise1.kr(1,0.49,0.5));
	
	local = Rotate2.ar(local[0], local[1], 0.237);
	local = AllpassN.ar(local, 0.05, {Rand(0.01,0.05)}.dup, 2);
	local = DelayN.ar(local, 0.3, {Rand(0.15,0.33)}.dup);
	local = AllpassN.ar(local, 0.05, {Rand(0.03,0.15)}.dup, 2);
	local = LeakDC.ar(local);	
	local = local + in;

	4.do { var t;
		t = {Rand(0.005,0.02)}.dup;
		local = AllpassN.ar(local, t, t, 1);
	};
	
	LocalOut.ar(local);
	Out.ar(0, local)
	}).send(s)
)
;
/////////////

/*
p.pop;

~fx = Synth.tail(s, \flanger)
~fx.free;
~fx.set(\fback, 8, \mix, 0.02)

~fx2.free;

~fx3 = Synth.tail(s, \bitRound)
~fx3.set(\round, 0.16, \amp, 0.9)
~fx3.free;

~fx4 = Synth.tail(s, \verb)
~fx4.set(\wet, 1.9)
~fx4.free;


~fx5 = Synth.tail(s, \bitRound)
~fx5.set(\round, 0.03, \amp, 0.618)
~fx5.free


////////////

*/