#!/bin/bash
# This script (re)starts darkice and connect to Supercollider

DATE=`date +%y%m%d-%H:%M`

# Are we alone?
if ! ps -C jackd > /dev/null; then
    echo "$DATE - Jack not running, kthxbye."
    exit 0
else
    echo "$DATE - Jack is running, good."
fi
if ! ps -C sclang > /dev/null; then
    echo "$DATE - Supercollider is not running, kthxbye."
    exit 0
else
    echo "$DATE - Supercollider is running, good."
fi
if ! jack_lsp | grep SuperCollider > /dev/null; then
    echo "$DATE - Supercollider is not connected to Jack, kthxbye."
    exit 0
else
    echo "$DATE - Supercollider is connected to Jack, good."
fi


# Is darkice already running?
if ! ps -C darkice > /dev/null; then 
    echo "$DATE - Darkice is not running"
    echo "$DATE - Attempting to start"
    darkice -c ~/darkice-mode+v.cfg&
    sleep 0.5
    jack_connect SuperCollider:out_1 darkice-`pidof darkice`:left
    jack_connect SuperCollider:out_2 darkice-`pidof darkice`:right
    if ps -C darkice > /dev/null; then
	echo "$DATE - Start succesful :)"
    else
	echo "$DATE - Start failed :("
	exit 0
    fi
else
    echo "$DATE - Darkice already running"
    echo "$DATE - Attempting to restart"
    kill -9 `pidof darkice`
    darkice -c ~/darkice-mode+v.cfg&
    sleep 0.5
    jack_connect SuperCollider:out_1 darkice-`pidof darkice`:left
    jack_connect SuperCollider:out_2 darkice-`pidof darkice`:right
    if ps -C darkice > /dev/null; then
	echo "$DATE - restart succesful :)"
    else
	echo "$DATE - Start failed :("
	exit 0
    fi
    # | mail -s "The darkice process went down" $RECIPIENT;
fi